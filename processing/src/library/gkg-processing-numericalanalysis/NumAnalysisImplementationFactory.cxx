#include <gkg-processing-numericalanalysis/NumAnalysisImplementationFactory.h>
#include <gkg-processing-numericalanalysis/Matrix.h>
#include <gkg-processing-numericalanalysis/Vector.h>
#include <gkg-processing-numericalanalysis/CMatrix.h>
#include <gkg-processing-numericalanalysis/PowellNewUOAFunction.h>
#include <gkg-processing-algobase/Math.h>
#include <gkg-core-cppext/Limits.h>
#include <gkg-core-exception/Exception.h>
#include <cmath>
#include <algorithm>
#include <limits>

#include <gkg-processing-numericalanalysis/PowellNewUOA_i.h>
#include <gkg-processing-numericalanalysis/UnitPseudoNormalVector_i.h>


gkg::NumericalAnalysisImplementationFactory::
     NumericalAnalysisImplementationFactory()
{
}


gkg::NumericalAnalysisImplementationFactory::
     ~NumericalAnalysisImplementationFactory()
{
}


double gkg::NumericalAnalysisImplementationFactory::getMinimumItem(
                                                      const gkg::Vector& vector,
                                                      int32_t* index ) const
{

  try
  {

    int32_t i, size = vector.getSize();
    int32_t bestIndex = 0;
    double bestValue = vector( 0 );
    
    for ( i = 1; i < size; i++ )
    {

      if ( vector( i ) < bestValue )
      {

        bestIndex = i;
        bestValue = vector( i );

      }

    }
    if ( index )
    {

      *index = bestIndex;

    }
    return bestValue;

  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::getMinimumItem( "
             "const gkg::Vector& vector, "
             "int* index ) const" );

}


double gkg::NumericalAnalysisImplementationFactory::getMaximumItem(
                                                      const gkg::Vector& vector,
                                                      int32_t* index ) const
{

  try
  {

    int32_t i, size = vector.getSize();
    int32_t bestIndex = 0;
    double bestValue = vector( 0 );
    
    for ( i = 1; i < size; i++ )
    {

      if ( vector( i ) > bestValue )
      {

        bestIndex = i;
        bestValue = vector( i );

      }

    }
    if ( index )
    {

      *index = bestIndex;

    }
    return bestValue;

  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::getMaximumItem( "
             "const gkg::Vector& vector, "
             "int* index ) const" );

}


double gkg::NumericalAnalysisImplementationFactory::getAbsMinimumItem(
                                                      const gkg::Vector& vector,
                                                      int32_t* index ) const
{

  try
  {

    int32_t i, size = vector.getSize();
    int32_t bestIndex = 0;
    double bestValue = std::fabs( vector( 0 ) );
    
    for ( i = 1; i < size; i++ )
    {

      if ( std::fabs( vector( i ) ) < bestValue )
      {

        bestIndex = i;
        bestValue = std::fabs( vector( i ) );

      }

    }
    if ( index )
    {

      *index = bestIndex;

    }
    return bestValue;

  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::getAbsMinimumItem( "
             "const gkg::Vector& vector, "
             "int* index ) const" );

}


double gkg::NumericalAnalysisImplementationFactory::getAbsMaximumItem(
                                                      const gkg::Vector& vector,
                                                      int32_t* index ) const
{

  try
  {

    int32_t i, size = vector.getSize();
    int32_t bestIndex = 0;
    double bestValue = std::fabs( vector( 0 ) );
    
    for ( i = 1; i < size; i++ )
    {

      if ( std::fabs( vector( i ) ) > bestValue )
      {

        bestIndex = i;
        bestValue = std::fabs( vector( i ) );

      }

    }
    if ( index )
    {

      *index = bestIndex;

    }
    return bestValue;

  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::getAbsMaximumItem( "
             "const gkg::Vector& vector, "
             "int* index ) const" );

}


double gkg::NumericalAnalysisImplementationFactory::getMinimumItem(
                                                      const gkg::Matrix& matrix,
                                                      int32_t* index1,
                                                      int32_t* index2 ) const
{

  try
  {

    int32_t i1, i2, size1 = matrix.getSize1(), size2 = matrix.getSize2();
    int32_t bestIndex1 = 0;
    int32_t bestIndex2 = 0;
    double bestValue = matrix( 0, 0 );
    
    for ( i1 = 1; i1 < size1; i1++ )
    {

      for ( i2 = 1; i2 < size2; i2++ )
      {

        if ( matrix( i1, i2 ) < bestValue )
        {

          bestIndex1 = i1;
          bestIndex2 = i2;
          bestValue = matrix( i1, i2 );

        }

      }

    }
    if ( index1 && index2 )
    {

      *index1 = bestIndex1;
      *index2 = bestIndex2;

    }
    return bestValue;

  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::getMinimumItem( "
             "const gkg::Matrix& matrix, "
             "int* index1, "
             "int* index2 ) const" );

}


double gkg::NumericalAnalysisImplementationFactory::getMaximumItem(
                                                      const gkg::Matrix& matrix,
                                                      int32_t* index1,
                                                      int32_t* index2 ) const
{

  try
  {

    int32_t i1, i2, size1 = matrix.getSize1(), size2 = matrix.getSize2();
    int32_t bestIndex1 = 0;
    int32_t bestIndex2 = 0;
    double bestValue = matrix( 0, 0 );
    
    for ( i1 = 1; i1 < size1; i1++ )
    {

      for ( i2 = 1; i2 < size2; i2++ )
      {

        if ( matrix( i1, i2 ) > bestValue )
        {

          bestIndex1 = i1;
          bestIndex2 = i2;
          bestValue = matrix( i1, i2 );

        }

      }

    }
    if ( index1 && index2 )
    {

      *index1 = bestIndex1;
      *index2 = bestIndex2;

    }
    return bestValue;

  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::getMaximumItem( "
             "const gkg::Matrix& matrix, "
             "int* index1, "
             "int* index2 ) const" );

}


double gkg::NumericalAnalysisImplementationFactory::getAbsMinimumItem(
                                                      const gkg::Matrix& matrix,
                                                      int32_t* index1,
                                                      int32_t* index2 ) const
{

  try
  {

    int32_t i1, i2, size1 = matrix.getSize1(), size2 = matrix.getSize2();
    int32_t bestIndex1 = 0;
    int32_t bestIndex2 = 0;
    double bestValue = std::fabs( matrix( 0, 0 ) );
    
    for ( i1 = 1; i1 < size1; i1++ )
    {

      for ( i2 = 1; i2 < size2; i2++ )
      {

        if ( std::fabs( matrix( i1, i2 ) ) < bestValue )
        {

          bestIndex1 = i1;
          bestIndex2 = i2;
          bestValue = std::fabs( matrix( i1, i2 ) );

        }

      }

    }
    if ( index1 && index2 )
    {

      *index1 = bestIndex1;
      *index2 = bestIndex2;

    }
    return bestValue;

  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::getAbsMinimumItem( "
             "const gkg::Matrix& matrix, "
             "int* index1, "
             "int* index2 ) const" );

}


double gkg::NumericalAnalysisImplementationFactory::getAbsMaximumItem(
                                                      const gkg::Matrix& matrix,
                                                      int32_t* index1,
                                                      int32_t* index2 ) const
{

  try
  {

    int32_t i1, i2, size1 = matrix.getSize1(), size2 = matrix.getSize2();
    int32_t bestIndex1 = 0;
    int32_t bestIndex2 = 0;
    double bestValue = std::fabs( matrix( 0, 0 ) );
    
    for ( i1 = 1; i1 < size1; i1++ )
    {

      for ( i2 = 1; i2 < size2; i2++ )
      {

        if ( std::fabs( matrix( i1, i2 ) ) > bestValue )
        {

          bestIndex1 = i1;
          bestIndex2 = i2;
          bestValue = std::fabs( matrix( i1, i2 ) );

        }

      }

    }
    if ( index1 && index2 )
    {

      *index1 = bestIndex1;
      *index2 = bestIndex2;

    }
    return bestValue;

  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::getAbsMaximumItem( "
             "const gkg::Matrix& matrix, "
             "int* index1, "
             "int* index2 ) const" );

}


void gkg::NumericalAnalysisImplementationFactory::getSingularValueNthOrderFit(
                                                          const gkg::Vector& x,
                                                          const gkg::Vector& y,
                                                          gkg::Vector& a ) const
{

  try
  {

    int32_t lineCount = y.getSize();
    int32_t columnCount = a.getSize();

    // sanity check
    if ( lineCount < columnCount )
    {

      throw std::runtime_error(
              "'y' vector should have at least as much samples as vector 'a'" );

    }

    gkg::Matrix X( lineCount, columnCount );

    int32_t line, column;
    for ( line = 0; line < lineCount; line++ )
    {

      for ( column = 0; column < columnCount; column++ )
      {


        if ( ( column == 0 ) && ( std::fabs( x( line ) ) < 1e-6 ) )
        {

          // check to be sure we do not call pow( 0, 0 )
          X( line, column ) = 1.0;
 
        }
        else
        {

          X( line, column ) = std::pow( x( line ), ( double )column );

        }


      }

    }

    gkg::Matrix V( columnCount, columnCount );
    gkg::Vector S( columnCount );

    getSingularValueDecomposition( X, V, S );
    getSingularValueSolution( X, S, V, y, a );

  }
  GKG_CATCH( "int gkg::NumericalAnalysisImplementationFactory::"
             "getSingularValueNthOrderFit( "
             "const gkg::Vector& x, "
             "const gkg::Vector& y, "
             "gkg::Vector& a ) const" );

}


double 
gkg::NumericalAnalysisImplementationFactory::getConditionNumber(
                                                    const gkg::Matrix& A ) const
{

  try
  {

    int32_t size1 = A.getSize1();
    int32_t size2 = A.getSize2();
    int32_t minimumSize = std::min( size1, size2 );
    
    // processing SVD decomposition
    gkg::Matrix U( size1, size1 );
    gkg::Vector S( minimumSize );
    gkg::Matrix V( size2, size2 );

    getSingularValueDecomposition2( A, U, S, V );

    double conditionNumber;
    
    if ( S( minimumSize - 1 ) < ( double )0.000000000000000001 )
    {

      conditionNumber = 0.0;

    }
    else
    {

      conditionNumber = S( 0 ) / S( minimumSize - 1 );

    }
    return conditionNumber;

  }
  GKG_CATCH( "double gkg::NumericalAnalysisImplementationFactory::"
             "getConditionNumber( "
             "const gkg::Matrix& A ) const" );

}


double 
gkg::NumericalAnalysisImplementationFactory::getFrobeniusNorm(
                                                    const gkg::Matrix& A ) const
{

  try
  {

    int32_t size1 = A.getSize1();
    int32_t size2 = A.getSize2();
    int32_t minimumSize = std::min( size1, size2 );
    
    // processing SVD decomposition
    gkg::Matrix U( size1, size1 );
    gkg::Vector S( minimumSize );
    gkg::Matrix V( size2, size2 );

    getSingularValueDecomposition2( A, U, S, V );

    double frobeniusNorm = 0.0;
    int32_t i = 0;
    for ( i = 0; i < minimumSize; i++ )
    {

      frobeniusNorm += S( i ) * S( i );

    }
    return frobeniusNorm;

  }
  GKG_CATCH( "double gkg::NumericalAnalysisImplementationFactory::"
             "getFrobeniusNorm( "
             "const gkg::Matrix& A ) const" );

}


double 
gkg::NumericalAnalysisImplementationFactory::getEuclideanNorm( 
                                                    const gkg::Matrix& A ) const
{

  try
  {

    int32_t size1 = A.getSize1();
    int32_t size2 = A.getSize2();
    int32_t minimumSize = std::min( size1, size2 );
    
    // processing SVD decomposition
    gkg::Matrix U( size1, size1 );
    gkg::Vector S( minimumSize );
    gkg::Matrix V( size2, size2 );

    getSingularValueDecomposition2( A, U, S, V );

    return S( 0 );

  }
  GKG_CATCH( "double gkg::NumericalAnalysisImplementationFactory::"
             "getEuclideanNorm( "
             "const gkg::Matrix& A ) const" );

}


void gkg::NumericalAnalysisImplementationFactory::getMoorePenrosePseudoInverse(
                                                      const gkg::Matrix& A,
                                                      gkg::Matrix& Aplus ) const
{

  try
  {

    int32_t size1 = A.getSize1();
    int32_t size2 = A.getSize2();
    int32_t size = std::min( size1, size2 );

    gkg::Matrix U( size1, size1 );
    gkg::Vector S( size );
    gkg::Matrix V( size2, size2 );

    getSingularValueDecomposition2( A, U, S, V );

    gkg::Matrix Splus( size2, size1 );
    Splus.setZero();

    int32_t s;
    for ( s = 0; s < size; s++ )
    {

      Splus( s, s ) = ( S( s ) == 0.0 ? 0.0 : ( 1.0 / S( s ) ) );

    }

    Aplus = V.getComposition( Splus ).getComposition( U.getTransposition() );

  }
  GKG_CATCH( "void gkg::NumericalAnalysisImplementationFactory::"
             "getMoorePenrosePseudoInverse( "
             "const gkg::Matrix& A, "
             "gkg::Matrix& Aplus ) const" );

}


void gkg::NumericalAnalysisImplementationFactory::getMoorePenrosePseudoInverse(
                                                      const gkg::CMatrix& A,
                                                      gkg::CMatrix& Aplus ) const
{

  try
  {

    int32_t size1 = A.getSize1();
    int32_t size2 = A.getSize2();
    int32_t size = std::min( size1, size2 );

    gkg::CMatrix U( size1, size1 );
    gkg::Vector S( size );
    gkg::CMatrix V( size2, size2 );

    getESingularValueDecomposition( A, U, S, V );

    gkg::Matrix Splus( size2, size1 );
    Splus.setZero();

    int32_t s;
    for ( s = 0; s < size; s++ )
    {

      Splus( s, s ) = ( S( s ) == 0.0 ? 0.0 : ( 1.0 / S( s ) ) );

    }

    Aplus = V.getComposition( Splus ).getComposition(
                                                U.getHermitianTransposition() );

  }
  GKG_CATCH( "void gkg::NumericalAnalysisImplementationFactory::"
             "getMoorePenrosePseudoInverse( "
             "const gkg::CMatrix& A, "
             "gkg::CMatrix& Aplus ) const" );

}


void gkg::NumericalAnalysisImplementationFactory::getThikonovPseudoInverse(
                                                    const gkg::Matrix& A,
                                                    const gkg::Matrix& R,
                                                    double regularizationFactor,
                                                    gkg::Matrix& Aplus ) const
{

  try
  {

    int32_t size1 = A.getSize1();
    int32_t size2 = A.getSize2();

    // processing Aprime = ( AtA+lL)
    gkg::Matrix Aprime( size2, size2 );
    int32_t i1, i2, k;
    for ( i1 = 0; i1 < size2; i1++ )
    {

      for ( i2 = 0; i2 < size2; i2++ )
      {

        Aprime( i1, i2 ) = 0;
        for ( k = 0; k < size1; k++ )
        {

          Aprime( i1, i2 ) += A( k, i1 ) * A( k, i2 );


        }
        Aprime( i1, i2 ) += regularizationFactor * R( i1, i2 );

      }


    }

    // processing Ainv, inverse of Aprime
    gkg::Matrix U( size2, size2 );
    gkg::Vector S( size2 );
    gkg::Matrix V( size2, size2 );

    getSingularValueDecomposition2( Aprime, U, S, V );

    gkg::Matrix Splus( size2, size2 );
    Splus.setZero();
    int32_t s;
    for ( s = 0; s < size2; s++ )
    {

      Splus( s, s ) = ( S( s ) == 0.0 ? 0.0 : ( 1.0 / S( s ) ) );

    }

    gkg::Matrix Ainv;
    Ainv = V.getComposition( Splus ).getComposition( U.getTransposition() );

    // processing Aplus, regularized inverse of A
    Aplus = Ainv.getComposition( A.getTransposition() );

  }
  GKG_CATCH( "void gkg::NumericalAnalysisImplementationFactory::"
             "getThikonovPseudoInverse( "
             "const gkg::Matrix& A, "
             "const gkg::Matrix& R, "
             "double regularizationFactor, "
             "gkg::Matrix& Aplus ) const" );

}


void gkg::NumericalAnalysisImplementationFactory::getPower( 
						     const gkg::Matrix& A,
						     double power,
						     gkg::Matrix& Apower ) const
{

  try
  {

    int32_t size1 = A.getSize1();
    int32_t size2 = A.getSize2();
    int32_t minimumSize = std::min( size1, size2 );

    if ( size1 >= size2 )
    {

      Apower.reallocate( size1, size2 );

      gkg::Matrix U( size1, size1 );
      gkg::Vector S( minimumSize );
      gkg::Matrix V( size2, size2 );

      getSingularValueDecomposition2( A, U, S, V );

      int32_t s = 0;
      if ( power < 0 )
      {

        for ( s = 0; s < minimumSize; s++ )
        {

          S( s ) = ( ( S( s ) > 1e-50 ) ? ( 1.0 / S( s ) ) : 0.0 );

        }
        power = -power;

      }
      if ( !gkg::equal( power - 1.0, 0.0, 1e-300 ) ) 
      {

        for ( s = 0; s < minimumSize; s++ )
        {

          S( s ) = std::pow( S( s ), power );

        }

      }

      Apower = U.getComposition( S.getDiagonal() ).getComposition(
                                                         V.getTransposition() );

    }
    else
    {

      gkg::Matrix At = A.getTransposition();
      getPower( At, power, Apower );
      Apower.transpose();

    }

  }
  GKG_CATCH( "void gkg::NumericalAnalysisImplementationFactory::"
             "getPower( "
             "const gkg::Matrix& A, "
             "double power, "
             "gkg::Matrix& Apower ) const" );

}


double 
gkg::NumericalAnalysisImplementationFactory::getConditionNumber(
                                                   const gkg::CMatrix& A ) const
{

  try
  {

    int32_t size1 = A.getSize1();
    int32_t size2 = A.getSize2();
    int32_t minimumSize = std::min( size1, size2 );
    
    // processing SVD decomposition
    gkg::CMatrix U( size1, size1 );
    gkg::Vector S( minimumSize );
    gkg::CMatrix V( size2, size2 );

    getESingularValueDecomposition( A, U, S, V );

    double conditionNumber;
    
    if ( S( minimumSize - 1 ) < ( double )0.000000000000000001 )
    {

      conditionNumber = 0.0;

    }
    else
    {

      conditionNumber = S( 0 ) / S( minimumSize - 1 );

    }
    return conditionNumber;

  }
  GKG_CATCH( "double gkg::NumericalAnalysisImplementationFactory::"
             "getConditionNumber( "
             "const gkg::CMatrix& A ) const" );

}


double 
gkg::NumericalAnalysisImplementationFactory::getFrobeniusNorm(
                                                   const gkg::CMatrix& A ) const
{

  try
  {

    int32_t size1 = A.getSize1();
    int32_t size2 = A.getSize2();
    int32_t minimumSize = std::min( size1, size2 );
    
    // processing SVD decomposition
    gkg::CMatrix U( size1, size1 );
    gkg::Vector S( minimumSize );
    gkg::CMatrix V( size2, size2 );

    getESingularValueDecomposition( A, U, S, V );

    double frobeniusNorm = 0.0;
    int32_t i = 0;
    for ( i = 0; i < minimumSize; i++ )
    {

      frobeniusNorm += S( i ) * S( i );

    }
    return frobeniusNorm;

  }
  GKG_CATCH( "double gkg::NumericalAnalysisImplementationFactory::"
             "getFrobeniusNorm( "
             "const gkg::CMatrix& A ) const" );

}


double 
gkg::NumericalAnalysisImplementationFactory::getEuclideanNorm(
                                                   const gkg::CMatrix& A ) const
{

  try
  {

    int32_t size1 = A.getSize1();
    int32_t size2 = A.getSize2();
    int32_t minimumSize = std::min( size1, size2 );
    
    // processing SVD decomposition
    gkg::CMatrix U( size1, size1 );
    gkg::Vector S( minimumSize );
    gkg::CMatrix V( size2, size2 );

    getESingularValueDecomposition( A, U, S, V );

    return S( 0 );

  }
  GKG_CATCH( "double gkg::NumericalAnalysisImplementationFactory::"
             "getEuclideanNorm( "
             "const gkg::CMatrix& A ) const" );

}


std::complex< float >
gkg::NumericalAnalysisImplementationFactory::arccos(
                                          const std::complex< float >& z ) const
{

  try
  {

    std::complex< double >
      zout = this->arccos( std::complex< double >( z.real(), z.imag() ) );
    return std::complex< float >( ( float )zout.real(), ( float )zout.imag() );

  }
  GKG_CATCH( "std::complex< float > "
             "gkg::NumericalAnalysisImplementationFactory::"
             "arccos( const std::complex< float >& z ) const" );

}


std::complex< float >
gkg::NumericalAnalysisImplementationFactory::arcsin(
                                          const std::complex< float >& z ) const
{

  try
  {

    std::complex< double >
      zout = this->arccos( std::complex< double >( z.real(), z.imag() ) );
    return std::complex< float >( ( float )zout.real(), ( float )zout.imag() );

  }
  GKG_CATCH( "std::complex< float > "
             "gkg::NumericalAnalysisImplementationFactory::"
             "arcsin( const std::complex< float >& z ) const" );

}


std::complex< float >
gkg::NumericalAnalysisImplementationFactory::arctan(
                                          const std::complex< float >& z ) const
{

  try
  {

    std::complex< double >
      zout = this->arccos( std::complex< double >( z.real(), z.imag() ) );
    return std::complex< float >( ( float )zout.real(), ( float )zout.imag() );

  }
  GKG_CATCH( "std::complex< float > "
             "gkg::NumericalAnalysisImplementationFactory::"
             "arctan( const std::complex< float >& z ) const" );

}


double gkg::NumericalAnalysisImplementationFactory::getFactorialFunction(
                                                               double x ) const
{

  try
  {

    double result = 1.0;

    double i = 0;
    for ( i = 1.0; i <= x; i += 1.0 )
    {

        result *= i;

    }

    return result;

  }
  GKG_CATCH( "double gkg::NumericalAnalysisImplementationFactory::"
             "getFactorialFunction( double x ) const" );

}


double gkg::NumericalAnalysisImplementationFactory::getDoubleFactorialFunction(
                                                               double x ) const
{

  try
  {

    double result = 1.0;

    int32_t i = 0;
    for ( i = ( int32_t )( x + 0.5 ); i >= 1; i -= 2 )
    {

        result *= ( double )i;

    }

    return result;

  }
  GKG_CATCH( "double gkg::NumericalAnalysisImplementationFactory::"
             "getDoubleFactorialFunction( double x ) const" );

}


double
gkg::NumericalAnalysisImplementationFactory::getHermitePolynomialFunction(
                                                         int32_t order,
                                                         const double& x ) const
{

  try
  {

    double result = 0.0;
    if ( order == 0 )
    {

      result = 1.0;

    }
    else if ( order == 1 )
    { 

      result = 2.0 * x;

    }
    else 
    {

      result = ( 2.0 * x * this->getHermitePolynomialFunction( order - 1, x )
                 - 2.0 * ( order - 1 ) * this->getHermitePolynomialFunction(
                                                               order - 2, x ) );

    }

    return result;

  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getHermitePolynomialFunction( "
             "int32_t order, "
             "const double& x ) const" );

}


float
gkg::NumericalAnalysisImplementationFactory::getHermitePolynomialFunction(
                                                         int32_t order,
                                                         const float& x ) const
{

  try
  {

    float result = 0.0;
    if ( order == 0 )
    {

      result = 1.0f;

    }
    else if ( order == 1 )
    { 

      result = 2.0f * x;

    }
    else 
    {

      result = ( 2.0f * x * this->getHermitePolynomialFunction( order - 1, x )
                 - 2.0f * ( order - 1 ) * this->getHermitePolynomialFunction(
                                                               order - 2, x ) );

    }

    return result;

  }
  GKG_CATCH( "float "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getHermitePolynomialFunction( "
             "int32_t order, "
             "const float& x ) const" );

}


float
gkg::NumericalAnalysisImplementationFactory::getRegularBesselFunctionJ0(
                                                          const float& x ) const
{

  try
  {

    return ( float )this->getRegularBesselFunctionJ0( ( double )x );

  }
  GKG_CATCH( "float "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getRegularBesselFunctionJ0( const float& x ) const" );

}


float
gkg::NumericalAnalysisImplementationFactory::getRegularBesselFunctionJ1(
                                                          const float& x ) const
{

  try
  {

    return ( float )this->getRegularBesselFunctionJ1( ( double )x );

  }
  GKG_CATCH( "float "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getRegularBesselFunctionJ1( const float& x ) const" );

}


float
gkg::NumericalAnalysisImplementationFactory::getRegularBesselFunctionI0(
                                                          const float& x ) const
{

  try
  {

    return ( float )this->getRegularBesselFunctionI0( ( double )x );

  }
  GKG_CATCH( "float "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getRegularBesselFunctionI0( const float& x ) const" );

}


float
gkg::NumericalAnalysisImplementationFactory::getRegularBesselFunctionI1(
                                                          const float& x ) const
{

  try
  {

    return ( float )this->getRegularBesselFunctionI1( ( double )x );

  }
  GKG_CATCH( "float "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getRegularBesselFunctionI1( const float& x ) const" );

}


float
gkg::NumericalAnalysisImplementationFactory::getRegularBesselFunctionIn(
                                        const int32_t& n, const float& x ) const
{

  try
  {

    return ( float )this->getRegularBesselFunctionIn( n, ( double )x );

  }
  GKG_CATCH( "float "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getRegularBesselFunctionIn( "
             "const int32_t& n, const float& x ) const" );

}


float 
gkg::NumericalAnalysisImplementationFactory::getZeroBesselDerivativeFunctionJ0(
                                           const uint32_t& rootIndex ) const
{

  try
  {

    if ( rootIndex > 20 )
    {

      throw std::runtime_error(
                               "root index must remains lower or equal to 20" );

    }

    static float table[ 21 ] =
    {
 
      0.0,                   // unused
      3.831705970207512,
      7.015586669815613,
      10.17346813506272,
      13.32369193631422,
      16.47063005087763,
      19.61585851046824,
      22.76008438059277,
      25.90367208761838,
      29.04682853491686,
      32.18967991097440,
      35.33230755008387,
      38.47476623477162,
      41.61709421281445,
      44.75931899765282,
      47.90146088718545,
      51.04353518357151,
      54.18555364106132,
      57.32752543790101,
      60.46945784534749,
      63.61135669848123

    };

    return table[ rootIndex ];


  }
  GKG_CATCH( "float "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getZeroBesselDerivativeFunctionJ0( "
             "const uint32_t& rootOrder ) const" );

}


float 
gkg::NumericalAnalysisImplementationFactory::getZeroBesselDerivativeFunctionJ1(
                                           const uint32_t& rootIndex ) const
{

  try
  {

    if ( rootIndex > 20 )
    {

      throw std::runtime_error(
                               "root index must remains lower or equal to 20" );

    }

    static float table[ 21 ] =
    {
 
      0.0,                   // unused
      1.841183781340659,
      5.331442773525033,
      8.536316366346285,
      11.70600490259206,
      14.86358863390903,
      18.01552786268180,
      21.16436985918879,
      24.31132685721078,
      27.45705057105925,
      30.60192297266909,
      33.74618289866738,
      36.88998740923681,
      40.03344405335068,
      43.17662896544882,
      46.31959756117391,
      49.46239113970276,
      52.60504111155668,
      55.74757179225101,
      58.89000229918570,
      62.03234787066199

    };

    return table[ rootIndex ];

  }
  GKG_CATCH( "float "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getZeroBesselDerivativeFunctionJ1( "
             "const uint32_t& rootOrder ) const" );

}


double
gkg::NumericalAnalysisImplementationFactory::getWatsonDistribution(
                        double kappa,
                        const gkg::Vector3d< float >& principalOrientation,
                        const gkg::Vector3d< float >& currentOrientation ) const
{

  try
  {
  
    double result = 0.0;

    if ( kappa < 0.0 )
    {

      throw std::runtime_error( "kappa parameter must be positive" );

    }

    double hypergeometricFunction1F1 = this->getHypergeometricFunction1F1(
                                                                       0.5,
                                                                       1.5,
                                                                       kappa );
    if ( hypergeometricFunction1F1 )
    {

      double dotProduct = ( double )principalOrientation.x *
                          ( double )currentOrientation.x +
                          ( double )principalOrientation.y *
                          ( double )currentOrientation.y +
                          ( double )principalOrientation.z *
                          ( double )currentOrientation.z;

      result = ( 1.0 / hypergeometricFunction1F1 ) * 
               std::exp( kappa * dotProduct * dotProduct );
						 
    }
    
    return result;
  
  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getWatsonDistribution( "
             "double kappa, "
             "const gkg::Vector3d< float >& principalOrientation, "
             "const gkg::Vector3d< float >& currentOrientation ) const" );
}


double
gkg::NumericalAnalysisImplementationFactory::getWatsonDistribution(
                       double kappa,
                       const gkg::Vector3d< double >& principalOrientation,
                       const gkg::Vector3d< double >& currentOrientation ) const
{

  try
  {
  
    double result = 0.0;

    if ( kappa < 0.0 )
    {

      throw std::runtime_error( "kappa parameter must be positive" );

    }

    double hypergeometricFunction1F1 = this->getHypergeometricFunction1F1(
                                                                       0.5,
                                                                       1.5,
                                                                       kappa );
    if ( hypergeometricFunction1F1 )
    {

      double dotProduct = principalOrientation.dot( currentOrientation );
      result = ( 1.0 / hypergeometricFunction1F1 ) * 
               std::exp( kappa * dotProduct * dotProduct );
						 
    }
    
    return result;
  
  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getWatsonDistribution( "
             "double kappa, "
             "const gkg::Vector3d< double >& principalOrientation, "
             "const gkg::Vector3d< double >& currentOrientation ) const" );
}


gkg::Vector
gkg::NumericalAnalysisImplementationFactory::
                              getCanonicalWatsonSphericalHarmonicsDecomposition(
               double kappa,
               int32_t maximumSphericalHarmonicsOrder,
               const std::vector< gkg::Vector3d< float > >& orientations ) const
{

  try
  {

    int32_t orientationCount = ( int32_t )orientations.size();

    gkg::Matrix B( orientationCount, maximumSphericalHarmonicsOrder + 1 );

    int32_t o = 0;
    int32_t l = 0;

    // building matrix of spherical harmonics basis
    double radial = 0.0;
    double theta = 0.0;
    double phi = 0.0;
    for ( o = 0; o < orientationCount; o++ )
    {

      const gkg::Vector3d< float >& orientation = orientations[ o ];
      this->getCartesianToSphericalCoordinates( ( double )orientation.x,
                                                ( double )orientation.y,
                                                ( double )orientation.z,
                                                radial,
                                                theta,
                                                phi );

      for ( l = 0; l <= maximumSphericalHarmonicsOrder; l++ )
      {

        B( o, l ) = 
                 std::real( this->getSphericalHarmonic( l, 0, phi, theta ) );

      }

    }

    // computing the matrix for spherical harmonics decomposition
    gkg::Matrix Bplus;
    this->getMoorePenrosePseudoInverse( B, Bplus );

    // computing values of canonical Watson distribution along provided
    // orientations
    gkg::Vector watsonDistribution( orientationCount );
    gkg::Vector3d< float > principalOrientation( 0.0, 0.0, 1.0 );
    for ( o = 0; o < orientationCount; o++ )
    {

      const gkg::Vector3d< float >& orientation = orientations[ o ];
      watsonDistribution( o ) = this->getWatsonDistribution(
                                                           kappa,
                                                           principalOrientation,
                                                           orientation );

    }

    return Bplus.getComposition( watsonDistribution );

  }
  GKG_CATCH( "gkg::Vector "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getCanonicalWatsonSphericalHarmonicsDecomposition( "
             "double kappa, "
             "int32_t maximumSphericalHarmonicsOrder, "
             "const std::vector< gkg::Vector3d< float > >& orientations ) "
             "const" );

}


void gkg::NumericalAnalysisImplementationFactory::getRandomWatsonOrientation(
                            double kappa,
                            const gkg::Vector3d< float >& principalOrientation,
                            const gkg::RandomGenerator& randomGenerator,
                            gkg::Vector3d< float >& orientation ) const
{

  try
  {

    gkg::Vector3d< double > orientationDouble;
    this->getRandomWatsonOrientation( kappa,
                                      gkg::Vector3d< double >(
                                          ( double )principalOrientation.x,
                                          ( double )principalOrientation.y,
                                          ( double )principalOrientation.z ),
                                      randomGenerator,
                                      orientationDouble );
    orientation.x = ( float )orientationDouble.x;
    orientation.y = ( float )orientationDouble.y;
    orientation.z = ( float )orientationDouble.z;

  }
  GKG_CATCH( "void gkg::NumericalAnalysisImplementationFactory::"
             "getRandomWatsonOrientation( "
             "double kappa, "
             "const gkg::Vector3d< float >& principalOrientation, "
             "const gkg::RandomGenerator& randomGenerator, "
             "gkg::Vector3d< float >& orientation ) const" );

}


void gkg::NumericalAnalysisImplementationFactory::getRandomWatsonOrientation(
                            double kappa,
                            const gkg::Vector3d< double >& principalOrientation,
                            const gkg::RandomGenerator& randomGenerator,
                            gkg::Vector3d< double >& orientation ) const
{

  try
  {

    // get mean orientation in spherical coordinates 
    double norm = principalOrientation.getNorm();
    double alpha = std::acos( principalOrientation.z / norm );
    double beta = std::atan2( principalOrientation.y,
                              principalOrientation.x );

    // simulate Watson distribution
    double c = 1.0 / ( std::exp( ( double )kappa ) - 1.0 ); 
    double u = 0.0;
    double v = 0.0;
    double s = 0.0;
    double e = -1.0;

    while ( v > e )
    {

      u = this->getUniformRandomNumber( randomGenerator, 0.0, 1.0 );

      v = this->getUniformRandomNumber( randomGenerator, 0.0, 1.0 );

      s = ( 1.0 / kappa ) * std::log( u / c + 1.0 );

      e = std::exp( kappa * s * ( s - 1.0 ) );

    }

    double theta = std::acos( s );

    double r0 = this->getUniformRandomNumber( randomGenerator, 0.0, 1.0 );
    double phi = 2.0 * M_PI * r0;

    // generate simulated orientation 
    double x = std::sin( theta ) * std::cos( phi );
    double y = std::sin( theta ) * std::sin( phi );
    double z = std::cos( theta );

    // rotate simulated orientation according to input mean orientation
    orientation.x = std::cos( alpha ) * std::cos( beta ) * x +
                    std::cos( alpha ) * std::sin( beta ) * y -
                    std::sin( alpha ) * z;
    orientation.y = -std::sin( beta ) * x + std::cos( beta ) * y;
    orientation.z = std::sin( alpha ) * std::cos( beta ) * x + 
                    std::sin( alpha ) * std::sin( beta ) * y +
                    std::cos( alpha ) * z; 

    orientation.normalize();

    if ( kappa >= 1e6 )
    {

      orientation = principalOrientation;

    }

    // make sure that the orientation is the same direction as mean
    // due to z axis indetermination
    if ( ( principalOrientation.x * orientation.x ) < 0.0 )
    {

      orientation.x *= -1.0;

    }

  }
  GKG_CATCH( "void gkg::NumericalAnalysisImplementationFactory::"
             "getRandomWatsonOrientation( "
             "double kappa, "
             "const gkg::Vector3d< double >& principalOrientation, "
             "const gkg::RandomGenerator& randomGenerator, "
             "gkg::Vector3d< double >& orientation ) const" );

}


double gkg::NumericalAnalysisImplementationFactory::
                                   getWatsonKappaParameterFromAngularDispersion(
                                                double angularDispersion, 
                                                bool degrees,
                                                int32_t orientationCount,
                                                double epsilon ) const
{

  try
  {

    double targetAngularDispersion = angularDispersion;
    double targetEpsilon = epsilon;
    if ( degrees )
    {

      targetAngularDispersion *= M_PI / 180.0;
      targetEpsilon *= M_PI / 180.0;

    }

    gkg::RandomGenerator randomGenerator( gkg::RandomGenerator::Taus );

    gkg::Vector3d< float > zAxis( 0.0f, 0.0f, 1.0f );
    gkg::Vector3d< float > orientation;
    int32_t o = 0;
    double lowerKappa = 0.0;
    double medianKappa = 500.0;
    double upperKappa = 1000.0;

    double currentAngularDispersion = targetAngularDispersion + 
                                      2 * targetEpsilon;
    double currentAngularDispersionForLowerKappa = 0.0;
    double currentAngularDispersionForMedianKappa = 0.0;
    double currentAngularDispersionForUpperKappa = 0.0;

    double discrepancyOfAngularDispersionForLowerKappa = 0.0;
    double discrepancyOfAngularDispersionForMedianKappa = 0.0;
    double discrepancyOfAngularDispersionForUpperKappa = 0.0;

    bool lowerKappaIsBetter = false;
    double bestKappa = 0.0;

    while ( std::fabs( targetAngularDispersion - currentAngularDispersion ) >
            targetEpsilon )
    {

      // computing orientation distribution for lower kappa
      currentAngularDispersionForLowerKappa = 0.0; 
      for ( o = 0; o < orientationCount; o++ )
      {

        this->getRandomWatsonOrientation( lowerKappa,
                                          zAxis,
                                          randomGenerator,
                                          orientation );
        currentAngularDispersionForLowerKappa += 
                                       gkg::getLineAngles( orientation, zAxis );

      }
      currentAngularDispersionForLowerKappa /= ( double )orientationCount;

      // computing orientation distribution for median kappa
      currentAngularDispersionForMedianKappa = 0.0; 
      for ( o = 0; o < orientationCount; o++ )
      {

        this->getRandomWatsonOrientation( medianKappa,
                                          zAxis,
                                          randomGenerator,
                                          orientation );
        currentAngularDispersionForMedianKappa +=
                                       gkg::getLineAngles( orientation, zAxis );

      }
      currentAngularDispersionForMedianKappa /= ( double )orientationCount;

      // computing orientation distribution for upper kappa
      currentAngularDispersionForUpperKappa = 0.0; 
      for ( o = 0; o < orientationCount; o++ )
      {

        this->getRandomWatsonOrientation( upperKappa,
                                          zAxis,
                                          randomGenerator,
                                          orientation );
        currentAngularDispersionForUpperKappa += 
                                       gkg::getLineAngles( orientation, zAxis );

      }
      currentAngularDispersionForUpperKappa /= ( double )orientationCount;

      discrepancyOfAngularDispersionForLowerKappa =
        std::fabs( currentAngularDispersionForLowerKappa - 
                     targetAngularDispersion );
      discrepancyOfAngularDispersionForMedianKappa =
        std::fabs( currentAngularDispersionForMedianKappa - 
                     targetAngularDispersion );
      discrepancyOfAngularDispersionForUpperKappa =
        std::fabs( currentAngularDispersionForUpperKappa - 
                     targetAngularDispersion );

      if ( targetAngularDispersion >
           currentAngularDispersionForMedianKappa )
      {

        upperKappa = medianKappa;
        lowerKappaIsBetter = true;

      }
      else
      {

        lowerKappa = medianKappa;
        lowerKappaIsBetter = false;

      }      
      medianKappa = ( lowerKappa + upperKappa ) / 2.0;

      if ( lowerKappaIsBetter )
      {

        if ( discrepancyOfAngularDispersionForLowerKappa <
             discrepancyOfAngularDispersionForMedianKappa )
        {

          currentAngularDispersion = currentAngularDispersionForLowerKappa;
          bestKappa = lowerKappa;

        }
        else
        {

          currentAngularDispersion = currentAngularDispersionForMedianKappa;
          bestKappa = medianKappa;

        }

      }
      else
      {

        if ( discrepancyOfAngularDispersionForUpperKappa <
             discrepancyOfAngularDispersionForMedianKappa )
        {

          currentAngularDispersion = currentAngularDispersionForUpperKappa;
          bestKappa = upperKappa;

        }
        else
        {

          currentAngularDispersion = currentAngularDispersionForMedianKappa;
          bestKappa = medianKappa;

        }

      }

      //std::cout << "target=" << targetAngularDispersion << " "
      //          << "current=" << currentAngularDispersion << std::endl;
                

    }

    return bestKappa;

  }
  GKG_CATCH( "double gkg::NumericalAnalysisImplementationFactory::"
             "getWatsonKappaParameterFromAngularDispersion( "
             "double angularDispersion, "
             "bool degrees ) const" );


}


double gkg::NumericalAnalysisImplementationFactory::
                        getPrecomputedWatsonKappaParameterFromAngularDispersion(
                                                double angularDispersion, 
                                                bool degrees ) const
{

  try
  {

    double kappas[ 531 ] = { 0.343323,
                             0.305176,
                             0.37384,
                             0.38147,
                             0.404358,
                             0.457764,
                             0.434875,
                             0.442505,
                             0.465393,
                             0.244141,
                             0.488281,
                             0.50354,
                             0.518799,
                             0.518799,
                             0.549316,
                             0.572205,
                             0.595093,
                             0.602722,
                             0.671387,
                             0.63324,
                             0.656128,
                             0.663757,
                             0.701904,
                             0.694275,
                             0.686646,
                             0.709534,
                             0.610352,
                             0.732422,
                             0.747681,
                             0.762939,
                             0.762939,
                             0.793457,
                             0.82016,
                             0.839233,
                             0.854492,
                             0.915527,
                             0.87738,
                             0.900269,
                             0.907898,
                             0.946045,
                             0.938416,
                             0.930786,
                             0.953674,
                             0.976562,
                             0.98484,
                             0.995636,
                             1.02234,
                             1.00708,
                             1.06812,
                             1.06049,
                             1.05286,
                             1.07574,
                             1.09863,
                             1.15967,
                             1.11389,
                             1.13297,
                             1.12915,
                             1.15967,
                             1.18256,
                             1.20544,
                             1.21307,
                             1.09863,
                             1.22833,
                             1.23596,
                             1.25885,
                             1.25122,
                             1.28555,
                             1.30463,
                             1.32751,
                             1.33514,
                             1.28174,
                             1.3504,
                             1.35803,
                             1.38092,
                             1.43433,
                             1.41907,
                             1.43433,
                             1.44958,
                             1.45721,
                             1.70898,
                             1.48773,
                             1.4801,
                             1.50299,
                             1.5564,
                             1.53351,
                             1.54114,
                             1.56593,
                             1.58691,
                             1.64795,
                             1.6098,
                             1.63269,
                             1.64032,
                             1.67847,
                             1.65558,
                             1.66321,
                             1.6861,
                             1.58691,
                             1.71661,
                             1.73187,
                             1.75476,
                             1.7395,
                             1.77002,
                             1.79291,
                             1.8158,
                             1.80817,
                             1.77002,
                             1.83868,
                             1.84631,
                             1.8692,
                             1.86157,
                             1.90353,
                             1.91498,
                             1.93787,
                             1.9455,
                             1.96069,
                             1.97601,
                             1.9989,
                             2.00653,
                             2.01364,
                             2.02179,
                             2.02942,
                             2.05231,
                             2.0752,
                             2.08282,
                             2.10571,
                             2.12097,
                             2.13623,
                             2.16675,
                             2.16293,
                             2.18201,
                             2.18582,
                             2.31934,
                             2.22015,
                             2.24304,
                             2.25067,
                             2.22778,
                             2.26593,
                             2.30408,
                             2.29645,
                             2.2583,
                             2.32697,
                             2.33459,
                             2.35748,
                             2.38037,
                             2.388,
                             2.39563,
                             2.42233,
                             2.44141,
                             2.68555,
                             2.45667,
                             2.48337,
                             2.50244,
                             2.53296,
                             2.53296,
                             2.53677,
                             2.56348,
                             2.62451,
                             2.59399,
                             2.59781,
                             2.62451,
                             2.65503,
                             2.65503,
                             2.66647,
                             2.68555,
                             2.68555,
                             2.71606,
                             2.71988,
                             2.74658,
                             2.74658,
                             2.76184,
                             2.78854,
                             2.80762,
                             2.86865,
                             2.82288,
                             2.84576,
                             2.83813,
                             2.87628,
                             2.88391,
                             2.9068,
                             2.92969,
                             2.92969,
                             2.97546,
                             2.97546,
                             3.02124,
                             3.0098,
                             3.0365,
                             3.04031,
                             3.11279,
                             3.07846,
                             3.0899,
                             3.10898,
                             3.13568,
                             3.12805,
                             3.1662,
                             3.05176,
                             3.19672,
                             3.2196,
                             3.22342,
                             3.26538,
                             3.25394,
                             3.27301,
                             3.23486,
                             3.31879,
                             3.34167,
                             3.34167,
                             3.38745,
                             3.37601,
                             3.39508,
                             3.41797,
                             3.44086,
                             3.46375,
                             3.46756,
                             3.48663,
                             3.49426,
                             3.53241,
                             3.479,
                             3.55911,
                             3.58582,
                             3.60107,
                             3.62396,
                             3.64685,
                             3.65067,
                             3.66211,
                             3.67737,
                             3.71552,
                             3.75366,
                             3.74985,
                             3.77655,
                             3.79314,
                             3.81088,
                             3.82233,
                             3.8147,
                             3.8681,
                             3.88336,
                             3.90312,
                             3.92914,
                             3.93677,
                             3.93677,
                             3.99017,
                             4.00925,
                             4.08936,
                             4.05121,
                             4.07028,
                             4.11987,
                             4.11224,
                             4.1275,
                             4.02832,
                             4.16565,
                             4.18854,
                             4.21143,
                             4.2572,
                             4.21143,
                             4.28009,
                             4.30298,
                             4.30298,
                             4.34875,
                             4.37546,
                             4.63867,
                             4.40979,
                             4.45557,
                             4.4632,
                             4.48608,
                             4.57764,
                             4.53186,
                             4.55475,
                             4.58145,
                             4.62341,
                             4.63867,
                             4.66156,
                             4.67682,
                             4.71115,
                             4.74548,
                             4.82178,
                             4.776,
                             4.82178,
                             4.82941,
                             4.86374,
                             4.88281,
                             4.92859,
                             4.97437,
                             4.95911,
                             4.94385,
                             5.02777,
                             5.05829,
                             5.07355,
                             5.10406,
                             5.00488,
                             5.15747,
                             5.18799,
                             5.23376,
                             5.31006,
                             5.2948,
                             5.34058,
                             5.35583,
                             5.61523,
                             5.41687,
                             5.46265,
                             5.47791,
                             5.49316,
                             5.53131,
                             5.5542,
                             5.59235,
                             5.7373,
                             5.66864,
                             5.69153,
                             5.67627,
                             5.75256,
                             5.82886,
                             5.82886,
                             5.85938,
                             5.89752,
                             5.93567,
                             5.92041,
                             6.01196,
                             6.05011,
                             6.09589,
                             6.14929,
                             6.19507,
                             6.2027,
                             6.24084,
                             6.31714,
                             6.32477,
                             6.36292,
                             6.43921,
                             6.44684,
                             6.5155,
                             6.53076,
                             6.5918,
                             6.62231,
                             6.66809,
                             6.7749,
                             6.74438,
                             6.80542,
                             6.8512,
                             6.92749,
                             6.89697,
                             7.01141,
                             7.06482,
                             7.09534,
                             7.15637,
                             7.26318,
                             7.23267,
                             7.32422,
                             7.37762,
                             7.4234,
                             7.47681,
                             7.5531,
                             7.61414,
                             7.64465,
                             7.70569,
                             7.76672,
                             7.8125,
                             7.90405,
                             7.94983,
                             8.01086,
                             8.0719,
                             8.16345,
                             8.22449,
                             8.05664,
                             8.3313,
                             8.48389,
                             8.5144,
                             8.56018,
                             8.65173,
                             8.69751,
                             9.27734,
                             8.86536,
                             8.95691,
                             9.15527,
                             9.1095,
                             9.20105,
                             9.52148,
                             9.35364,
                             9.42993,
                             9.53674,
                             9.70459,
                             8.78906,
                             9.79614,
                             9.93347,
                             10.0098,
                             10.1929,
                             10.2539,
                             10.3455,
                             10.4065,
                             10.5591,
                             10.6659,
                             10.7422,
                             10.8643,
                             10.9863,
                             11.1084,
                             11.4746,
                             11.3525,
                             11.4746,
                             11.6425,
                             11.7188,
                             11.8713,
                             11.9934,
                             12.1765,
                             12.2681,
                             12.5732,
                             12.619,
                             12.7258,
                             12.8174,
                             13.1226,
                             13.2141,
                             13.3972,
                             13.6108,
                             13.7024,
                             14.0381,
                             14.0686,
                             14.3433,
                             14.4348,
                             14.1602,
                             14.8621,
                             15.0146,
                             15.3198,
                             15.4419,
                             15.6555,
                             15.8691,
                             16.3574,
                             16.4795,
                             16.1133,
                             16.9678,
                             16.8457,
                             17.4561,
                             17.5781,
                             17.8223,
                             18.158,
                             18.4937,
                             18.6768,
                             19.0125,
                             19.4092,
                             19.5312,
                             19.8975,
                             20.1416,
                             20.5078,
                             20.9045,
                             21.2402,
                             21.6675,
                             22.2168,
                             22.3694,
                             22.7661,
                             23.3154,
                             23.6206,
                             23.9868,
                             24.9023,
                             24.6582,
                             24.4141,
                             25.8789,
                             25.8789,
                             26.6113,
                             27.2969,
                             27.8931,
                             28.5034,
                             29.1748,
                             29.6021,
                             29.7852,
                             30.9448,
                             31.6162,
                             31.7383,
                             32.9285,
                             33.5999,
                             34.3933,
                             35.1562,
                             36.0107,
                             36.8652,
                             37.5977,
                             38.5132,
                             39.4287,
                             40.4053,
                             41.3818,
                             42.4194,
                             43.4265,
                             44.6777,
                             45.7764,
                             47.6875,
                             48.2178,
                             49.5605,
                             50.8281,
                             52.1851,
                             53.2227,
                             54.9316,
                             56.6406,
                             58.5938,
                             60.3027,
                             61.7676,
                             63.7207,
                             65.6738,
                             67.9932,
                             70.3125,
                             72.7539,
                             74.707,
                             77.3926,
                             80.2002,
                             83.0078,
                             86.0596,
                             89.1113,
                             93.2617,
                             96.4355,
                             99.8535,
                             104.248,
                             108.887,
                             112.549,
                             117.432,
                             122.803,
                             128.174,
                             134.033,
                             140.812,
                             146.973,
                             155.273,
                             163.086,
                             170.654,
                             183.594,
                             190.43,
                             199.707,
                             210.938,
                             223.633,
                             236.328,
                             252.93,
                             267.578,
                             286.133,
                             307.617,
                             329.59,
                             353.516,
                             382.812,
                             414.062,
                             447.266,
                             490.234,
                             534.18,
                             589.844,
                             644.531 };



    double angularDispersionInDegrees = angularDispersion;
    if ( degrees == false )
    {

      angularDispersionInDegrees = angularDispersion * 180.0 / M_PI;

    }

    double angularDispersionInDegreesTimes10 = angularDispersionInDegrees *
                                               10.0;


    int32_t lowerIntegerAngularDispersionInDegreesTimes10 =
                                   ( int32_t )angularDispersionInDegreesTimes10;


    int32_t lowestIntegerAngularDispersionInDegreesTimes10 = 20;
    int32_t uppestIntegerAngularDispersionInDegreesTimes10 = 550;


    double kappa = 0.0;

    if ( lowerIntegerAngularDispersionInDegreesTimes10 <
         lowestIntegerAngularDispersionInDegreesTimes10 )
    {

      kappa =  700.0;

    }
    else if ( lowerIntegerAngularDispersionInDegreesTimes10 + 1 > 
              uppestIntegerAngularDispersionInDegreesTimes10 )
    {

      this->getWatsonKappaParameterFromAngularDispersion(
                                                angularDispersion, 
                                                degrees,
                                                100000,
                                                angularDispersion / 1000.0 );

    }
    else
    {

      double kappa1 = kappas[
                           529 -
                           ( lowerIntegerAngularDispersionInDegreesTimes10 -
                             lowestIntegerAngularDispersionInDegreesTimes10 ) ];
      double kappa2 = kappas[
                           529 -
                           ( lowerIntegerAngularDispersionInDegreesTimes10 -
                             lowestIntegerAngularDispersionInDegreesTimes10 +
                              1 ) ];
      double deltaAngularDispersion =
                        angularDispersionInDegreesTimes10 - 
                        ( double )lowerIntegerAngularDispersionInDegreesTimes10;

      kappa =  kappa1 + deltaAngularDispersion * ( kappa2 - kappa1 );

    }
    return kappa;

  }
  GKG_CATCH( "double gkg::NumericalAnalysisImplementationFactory::"
             "getPrecomputedWatsonKappaParameterFromAngularDispersion( "
             "double angularDispersion, "
             "bool degrees ) const" );

}


double
gkg::NumericalAnalysisImplementationFactory::getBinghamDistribution(
                  double /* kappa1 */,
                  double /* kappa2 */,
                  const gkg::Vector3d< float >& /* principalOrientation */,
                  const gkg::Vector3d< float >& /* secondaryOrientation */,
                  const gkg::Vector3d< float >& /* currentOrientation */ ) const
{

  try
  {
  
    double result = 0.0;

    ////to be written
    
    return result;
  
  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getBinghamDistribution( "
             "double kappa1, "
             "double kappa2, "
             "const gkg::Vector3d< float >& principalOrientation, "
             "const gkg::Vector3d< float >& secondaryOrientation, "
             "const gkg::Vector3d< float >& currentOrientation ) const" );
}


double
gkg::NumericalAnalysisImplementationFactory::getBinghamDistribution(
                 double /* kappa1 */,
                 double /* kappa2 */,
                 const gkg::Vector3d< double >& /* principalOrientation */,
                 const gkg::Vector3d< double >& /* secondaryOrientation */,
                 const gkg::Vector3d< double >& /* currentOrientation */ ) const
{

  try
  {
  
    double result = 0.0;

    ////to be written
    
    return result;
  
  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getBinghamDistribution( "
             "double kappa1, "
             "double kappa2, "
             "const gkg::Vector3d< double >& principalOrientation, "
             "const gkg::Vector3d< double >& secondaryOrientation, "
             "const gkg::Vector3d< double >& currentOrientation ) const" );
}


void gkg::NumericalAnalysisImplementationFactory::getRandomESAGOrientation( 
                            const std::pair< double, double >& gammas,
                            const gkg::Vector3d< double >& principalOrientation,
                            const gkg::RandomGenerator& randomGenerator,
                            gkg::Vector3d< double >& orientation ) const
{

  try
  {

    double muN = principalOrientation.getNorm();
    double mu1 = principalOrientation.x;
    double mu2 = principalOrientation.y;
    double mu3 = principalOrientation.z;
 
    double muNSq = muN * muN;
    double mu1Sq = mu1 * mu1;
    double mu2Sq = mu2 * mu2;
    double mu3Sq = mu3 * mu3;
    double mu0Sq = mu2 * mu2 + mu3 * mu3;

    double gamma1 = gammas.first;
    double gamma2 = gammas.second;

    double alpha = std::sqrt( gamma1 * gamma1 + gamma2 * gamma2 + 1.0) - 1.0;

    // compute 3x3 variance-covariance matrix V according to 
    // principalOrientation and gammas
    gkg::Matrix Vinv(3,3);

    Vinv( 0, 0 ) = 1.0 + ( gamma1 + alpha ) * mu0Sq / muNSq;
    Vinv( 0, 1 ) = gamma2 * mu3 / muN - ( gamma1 + alpha ) * mu1 * mu2 / muNSq;
    Vinv( 0, 2 ) = -( gamma1 + alpha ) * mu1 * mu3 / muNSq -
                   gamma2 * ( mu2 / muN + mu1 * mu3 / muNSq );
    Vinv( 1, 0 ) = -( gamma1 + alpha ) * mu1 * mu2 / muNSq + gamma2 * mu3 / muN;
    Vinv( 1, 1 ) = 1.0 + gamma1 * ( mu1Sq* mu2Sq / ( mu0Sq * muNSq ) - 
                                    mu3Sq / mu0Sq ) -
                   2.0 * gamma2 * mu1 * mu2 * mu3 / ( mu0Sq * muN ) + 
                   alpha * ( mu1Sq * mu2Sq / ( mu0Sq * muNSq ) + mu3Sq / mu0Sq);
    Vinv( 1, 2 ) = gamma1 * ( mu1Sq * mu2 * mu3 / ( mu0Sq * muNSq ) +
                              mu2 * mu3 / mu0Sq ) +
                   gamma2 * ( ( mu1 * mu2Sq - mu1 * mu3Sq ) /
                              ( mu0Sq  * muN ) ) +
                   alpha * ( mu1Sq * mu2 * mu3 / ( mu0Sq * muNSq ) -
                             mu2 * mu3 / mu0Sq );
    Vinv( 2, 0 ) = -( gamma1 + alpha ) * mu1 * mu3 / muNSq - gamma2 * mu2 / muN;
    Vinv( 2, 1 ) = gamma1 * ( mu1Sq * mu2 * mu3 / ( mu0Sq * muNSq ) +
                              mu2 * mu3 / mu0Sq ) +
                   gamma2 * ( ( mu1 * mu2Sq - mu1 * mu3Sq ) / ( mu0Sq * muN ) )+
                   alpha * ( mu1Sq * mu2 * mu3 / ( mu0Sq * muNSq ) -
                             mu2 * mu3 / mu0Sq );
    Vinv( 2, 2 ) = 1.0 + gamma1 * ( mu1Sq * mu3Sq / ( mu0Sq * muNSq ) - 
                                    mu2Sq / mu0Sq ) + 
                   2.0 * gamma2 * mu1 * mu2 * mu3 / ( mu0Sq * muN ) + 
                   alpha * ( mu1Sq * mu3Sq / ( mu0Sq * muNSq ) + 
                             mu2Sq / mu0Sq );


    gkg::Matrix V(3,3);
    this->getMoorePenrosePseudoInverse( Vinv, V );


    // simulate z ~ N( principalOrientation, V )
    int32_t seed =  ( int32_t )this->getUniformRandomUInt32(
                        randomGenerator,
                        ( uint32_t )( std::numeric_limits< int32_t >::max() ) );

    std::vector< double > a( 9 );
    a[ 0 ] = V( 0, 0 );
    a[ 1 ] = V( 0, 1 );
    a[ 2 ] = V( 0, 2 );
    a[ 3 ] = V( 1, 0 );
    a[ 4 ] = V( 1, 1 );
    a[ 5 ] = V( 1, 2 );
    a[ 6 ] = V( 2, 0 );
    a[ 7 ] = V( 2, 1 );
    a[ 8 ] = V( 2, 2 );

    std::vector< double > mu( 3 );
    mu[ 0 ] = principalOrientation.x;
    mu[ 1 ] = principalOrientation.y;
    mu[ 2 ] = principalOrientation.z;


    // first computing the Cholesky R factor of the variance-covariance matrix
    std::vector< double > R( 9 );

    int32_t i = 0;
    int32_t j = 0;
    int32_t k = 0;
    double s = 0.0;

    for ( j = 0; j < 3; j++ )
    {

      for ( i = 0; i < 3; i++ )
      {

        R[ i + j * 3 ] = a[ i + j * 3 ];

      }

    }

    for ( j = 0; j < 3; j++ )
    {

      for ( k = 0; k <= j - 1; k++ )
      {

        for ( i = 0; i <= k - 1; i++ )
        {

          R[ k + j * 3 ] = R[ k + j * 3 ] - R[ i + k * 3 ] * R[ i + j * 3 ];

        }
        R[ k + j * 3 ] = R[ k + j * 3 ] / R[ k + k * 3 ];

      }

      s = R[ j + j * 3 ];
      for ( i = 0; i <= j - 1; i++ )
      {

        s = s - R[ i + j * 3 ] * R[ i + j * 3 ];

      }

      if ( s <= 0.0 )
      {

        throw std::runtime_error(
                       "covariance matrix is not positive definite symmetric" );

      }

      R[ j + j * 3 ] = std::sqrt( s );

    }

    //  zero out the lower triangle.
    for ( i = 0; i < 3; i++ )
    {

      for ( j = 0; j < i; j++ )
      {

        R[ i + j * 3 ] = 0.0;

      }

    }

    //  Y = MxN matrix of samples of the 1D normal dist. with mean/var=0.0/1.0  
    double* y = r8vec_normal_01_new ( 3, &seed );


    // computing X = MU + R' * Y.
    std::vector< double > x( 3 );
    for ( i = 0; i < 3; i++ )
    {

      x[ i ] = mu[i];
      for ( k = 0; k < 3; k++ )
      {

        x[ i ] = x[ i ] + R[ k + i * 3 ] * y[ k ];

      }

    }

    delete [] y;

    // now copying x to the ouput orientation
    orientation.x = x[ 0 ];
    orientation.y = x[ 1 ];
    orientation.z = x[ 2 ];

    // set orientation = z / ||z||
    orientation.normalize();

  }
  GKG_CATCH( "void "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getRandomESAGOrientation( "
             "const std::pair< double, double >& gammas, "
             "const gkg::Vector3d< double >& principalOrientation, "
             "const gkg::RandomGenerator& randomGenerator, "
             "gkg::Vector3d< double >& orientation ) const" );

}


double 
gkg::NumericalAnalysisImplementationFactory::getPowellNewUOAOptimumParameters(
                                                gkg::Vector& parameters,
                                                gkg::PowellNewUOAFunction& f,
                                                double initialTrustRegionRadius,
                                                int32_t maximumIterationCount,
                                                double maximumTestSize,
                                                bool verbose ) const
{

  try
  {

    // collecting the parameter count
    int32_t parameterCount = parameters.getSize();
    if ( f.getParameterCount() != parameterCount )
    {

      throw std::runtime_error( "Powell's New UOA function and parameters have "
                                "incompatible size(s)" );

    }

    int32_t pointCount = 2 * parameterCount + 1;
    int32_t rnf = 0;

    std::vector< double > w( ( pointCount + 13 ) *
                             ( pointCount + parameterCount ) +
                             3 * parameterCount * ( parameterCount + 3 ) / 2 +
                             11 );

    return newuoa( parameterCount,
                   pointCount,
                   &parameters( 0 ),
                   initialTrustRegionRadius,
                   maximumTestSize,
                   &rnf,
                   maximumIterationCount,
                   &w[ 0 ],
                   f,
                   verbose );

  }
  GKG_CATCH( "int32_t "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getPowellNewUOAOptimumParameters( "
             "gkg::Vector& parameters, "
             "gkg::PowellNewUOAFunction& f, "
             "double initialTrustRegionRadius, "
             "int32_t maximumIterationCount, "
             "double maximumTestSize, "
             "bool verbose ) const" );

}



void 
gkg::NumericalAnalysisImplementationFactory::getCartesianToSphericalCoordinates(
                                                      double x,
                                                      double y,
                                                      double z,
                                                      double& radial,
                                                      double& azimuth,
                                                      double& colatitude ) const
{

  try
  {

    radial = std::sqrt( x * x + y * y + z * z );
    
    double zOverRadial = z / radial;
    if ( zOverRadial > 1.0 )
    {

      colatitude = 0.0;

    }
    else if ( zOverRadial < -1.0 )
    {

      colatitude = M_PI;

    }
    else
    {

      colatitude = std::acos( zOverRadial );
 
    }

    if ( x == 0.0 )
    {

      if ( y >= 0.0 )
      {

        azimuth = + M_PI / 2;

      }
      else
      {

        azimuth = - M_PI / 2;

      }

    }
    else
    {

      if ( ( y >= 0.0 ) && ( x > 0.0 ) )
      {

        azimuth = std::atan( y / x );

      }
      else if ( ( y <= 0.0 ) && ( x < 0.0 ) )
      {

        azimuth = std::atan( y / x ) + M_PI;

      } 
      else if ( ( y <= 0.0 ) && ( x > 0.0 ) )
      {

        azimuth = std::atan( y / x ) + 2 * M_PI;

      } 
      else if ( ( y >= 0.0 ) && ( x < 0.0 ) )
      {

        azimuth = std::atan( y / x ) + M_PI;

      } 

    }

  }
  GKG_CATCH( "void "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getCartesianToSphericalCoordinates( "
             "double x, "
             "double y, "
             "double z, "
             "double& radial, "
             "double& azimuth, "
             "double& colatitude ) const" );

}


void 
gkg::NumericalAnalysisImplementationFactory::getSphericalToCartesianCoordinates(
                                                      double radial,
                                                      double azimuth,
                                                      double colatitude,
                                                      double& x,
                                                      double& y,
                                                      double& z ) const
{

  try
  {

    x = radial * std::sin( colatitude ) * std::cos( azimuth );
    y = radial * std::sin( colatitude ) * std::sin( azimuth );
    z = radial * std::cos( colatitude );

  }
  GKG_CATCH( "void "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getSphericalToCartesianCoordinates( "
             "double radial, "
             "double azimuth, "
             "double colatitude, "
             "double& x, "
             "double& y, "
             "double& z ) const" );

}


double 
gkg::NumericalAnalysisImplementationFactory::getParzenWindowInterpolation(
                                                double x,
                                                const std::vector< double >& xs,
                                                const std::vector< double >& ys,
                                                double kernelWidth ) const
{

  try
  {

    // sanity check(s)
    if ( xs.size() != ys.size() )
    {

      throw std::runtime_error( "xs and ys have different size(s)" );

    }
    int32_t sampleCount = ( int32_t )xs.size();

    // computing the PDF value
    double value = 0;
    int32_t s = 0;
    for ( s = 0; s < sampleCount; s++ )
    {

      value += ys[ s ] *
               this->getGaussianPdf( ( x - xs[ s ] ) / kernelWidth,
                                     0.0, 1.0 );

    }
    return value / kernelWidth;

  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getParzenWindowInterpolation( "
             "double x, "
             "const std::vector< double >& xs, "
             "const std::vector< double >& ys, "
             "double kernelWidth ) const" );

}


double 
gkg::NumericalAnalysisImplementationFactory::getParzenWindowInterpolation(
                                                double /* x */,
                                                const gkg::Vector& xs,
                                                const gkg::Vector& ys,
                                                double kernelWidth ) const
{

  try
  {

    // sanity check(s)
    if ( xs.getSize() != ys.getSize() )
    {

      throw std::runtime_error( "xs and ys have different size(s)" );

    }
    int32_t sampleCount = ( int32_t )xs.getSize();

    // computing the PDF value
    double value = 0;
    int32_t s = 0;
    for ( s = 0; s < sampleCount; s++ )
    {

      value += ys( s ) * 
               this->getGaussianPdf( ( value - xs( s ) ) / kernelWidth,
                                     0.0, 1.0 );

    }
    return value / kernelWidth;

  }
  GKG_CATCH( "double "
             "gkg::NumericalAnalysisImplementationFactory::"
             "getParzenWindowInterpolation( "
             "double x, "
             "const std::vector< double >& xs, "
             "const std::vector< double >& ys, "
             "double kernelWidth ) const" );

}

