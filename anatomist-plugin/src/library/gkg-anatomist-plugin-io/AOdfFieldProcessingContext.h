#ifndef _gkg_anatomist_plugin_io_AOdfFieldProcessingContext_h_
#define _gkg_anatomist_plugin_io_AOdfFieldProcessingContext_h_


#include <gkg-communication-thread/LoopContext.h>
#include <gkg-dmri-container/OdfCartesianField.h>
#include <gkg-processing-coordinates/Vector3d.h>
#include <gkg-processing-transform/Translation3d.h>
#include <vector>

namespace gkg
{


class AOdfFieldProcessingContext : public LoopContext< int32_t >
{

  public:

    AOdfFieldProcessingContext(
                 OdfCartesianField* odfCartesianField,
                 std::vector< std::vector< float > >& transformations,
                 std::vector< Translation3d< float > >& translations,
                 float meshScale,
                 bool hasGFANormalization );
    virtual ~AOdfFieldProcessingContext();

    void doIt( int32_t startIndex, int32_t countIndex  );

  private:

    OdfCartesianField* _odfCartesianField;
    std::vector< std::vector< float > >& _transformations;
    std::vector< Translation3d< float > >& _translations;
    float _meshScale; 
    bool _hasGFANormalization; 
    Vector3d< double > _resolution;


};



}


#endif
