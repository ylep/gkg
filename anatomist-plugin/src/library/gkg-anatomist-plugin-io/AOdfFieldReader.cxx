#include <gkg-anatomist-plugin-io/AOdfFieldReader.h>
#include <gkg-anatomist-plugin-io/AOdfFieldProcessingContext.h>
#include <gkg-anatomist-plugin-io/TransformationsAndTranslationsProcessingContext.h>
#include <gkg-anatomist-plugin-io/TransformationsAndTranslationsAndNormalsProcessingContext.h>
#include <gkg-anatomist-plugin-io/NormalsProcessingContext.h>
#include <gkg-processing-container/SiteMap_i.h>
#include <gkg-processing-container/TextureMap_i.h>
#include <gkg-processing-container/MeshMap_i.h>
#include <gkg-processing-container/CartesianField_i.h>
#include <gkg-dmri-odf/OrientationDistributionFunction_i.h>
#include <gkg-processing-coordinates/ElectrostaticOrientationSet.h>
#include <gkg-processing-mesh/ConvexHull_i.h>
#include <gkg-core-io/Reader_i.h>
#include <gkg-communication-thread/ThreadedLoop.h>
#include <gkg-core-exception/Exception.h>
#include <iostream>


gkg::AOdfFieldReader::AOdfFieldReader( const std::string& fileNameSiteMap,
                                       const std::string& fileNameTextureMap,
                                       int32_t outputOrientationCount )
                     : _boundingBox( -1.0, -1.0, -1.0, -1.0, -1.0, -1.0 ),
                       _outputOrientationCount( outputOrientationCount ),
                       _isSphericalHarmonics( false ),
                       _hasGFANormalization( false ),
                       _meshScale( 1.0 )
{

  try
  {

    // reading the site and ODF texture map(s)
    gkg::Reader::getInstance().read( fileNameSiteMap, _siteMap );
    gkg::Reader::getInstance().read( fileNameTextureMap, _odfs );


    // checking that the texture map is not empty
    gkg::TextureMap< gkg::OrientationDistributionFunction >::iterator
      o = _odfs.begin(),
      oe = _odfs.end();

    if ( o == oe )
    {

      throw std::runtime_error( "ODF field is empty!" );

    }

    // checking for the basis type
    gkg::OrientationDistributionFunction::BasisType
      basisType = o->second.getBasisType();

    if ( basisType == gkg::OrientationDistributionFunction::SphericalHarmonics )
    {

      _isSphericalHarmonics = true;
      if ( _outputOrientationCount <= 0 )
      {

        throw std::runtime_error( "missing or bad output orientation count" );

      }
      // creating output orientation set
      _outputOrientationSet =  new gkg::OrientationSet(
                  gkg::ElectrostaticOrientationSet(
                   _outputOrientationCount / 2 ).getSymmetricalOrientations() );

    }
    else if ( basisType == gkg::OrientationDistributionFunction::Standard )
    {

      _outputOrientationCount = o->second.getValueCount();
      // creating output orientation set
      _outputOrientationSet =  new gkg::OrientationSet(
                  gkg::ElectrostaticOrientationSet(
                   _outputOrientationCount / 2 ).getSymmetricalOrientations() );

    }
    else if ( basisType == gkg::OrientationDistributionFunction::Shore )
    {

      _outputOrientationCount = o->second.getValueCount();
      // creating output orientation set
      _outputOrientationSet =  new gkg::OrientationSet(
                  gkg::ElectrostaticOrientationSet(
                   _outputOrientationCount / 2 ).getSymmetricalOrientations() );

    }

    if ( basisType == gkg::OrientationDistributionFunction::SphericalHarmonics )
    {

      o = _odfs.begin();
      while ( o != oe )
      {

        o->second.setOrientationSet( _outputOrientationSet );
        ++ o;

      }

    }

    // processing the ODF cartesian field
    _odfCartesianField = new gkg::OdfCartesianField( _siteMap,
                                                     _odfs,
                                                     *_outputOrientationSet );

    // processing the convex hull of a sphere containing a given number of
    // vertices
    gkg::ConvexHull::getInstance().addConvexHull(
                                       _outputOrientationSet->getOrientations(),
                                       0,
                                       _sphereMeshMap );

    // processing the ODF field
    computeOdfField();

    // computing bounding box
    computeBoundingBox();

  }
  GKG_CATCH( "gkg::AOdfFieldReader::AOdfFieldReader( "
             "const std::string& fileNameSiteMap, "
             "const std::string& fileNameTextureMap, "
             "int32_t outputOrientationCount )" );

}


gkg::AOdfFieldReader::~AOdfFieldReader()
{

  delete _odfCartesianField;
  delete _outputOrientationSet;

}


gkg::Vector3d< double > gkg::AOdfFieldReader::getResolution() const
{

  try
  {

    return _odfCartesianField->getResolution();

  }
  GKG_CATCH( "gkg::Vector3d< double > "
             "gkg::AOdfFieldReader::getResolution() const" );

}


const gkg::BoundingBox< float >& gkg::AOdfFieldReader::getBoundingBox() const
{

  try
  {

    return _boundingBox;

  }
  GKG_CATCH( "const gkg::BoundingBox< float >& "
             "gkg::AOdfFieldReader::getBoundingBox() const" );

}


bool gkg::AOdfFieldReader::isSphericalHarmonics() const
{

  try
  {

    return _isSphericalHarmonics;

  }
  GKG_CATCH( "bool gkg::AOdfFieldReader::isSphericalHarmonics() const" );

}


bool gkg::AOdfFieldReader::hasGFANormalization() const
{

  try
  {

    return _hasGFANormalization;

  }
  GKG_CATCH( "bool gkg::AOdfFieldReader::isSphericalHarmonics() const" );

}


int32_t gkg::AOdfFieldReader::getOutputOrientationCount() const
{

  try
  {

    return _outputOrientationCount;

  }
  GKG_CATCH( "int32_t getOutputOrientationCount()const" );

}


void gkg::AOdfFieldReader::getSphereVertexCoordinatesAndIndices(
                                          QVector< GLfloat >& vertexCoordinates,
                                          QVector<GLuint>& vertexIndices ) const
{

  try
  {

    // clearing structures
    vertexCoordinates.clear();
    vertexIndices.clear();

    // creating vertex coordinates
    const std::list< gkg::Vector3d< float > >&
      vertices = _sphereMeshMap.vertices.getSites( 0 );

    vertexCoordinates.resize( 3U * vertices.size() );

    std::list< gkg::Vector3d< float > >::const_iterator
      v = vertices.begin(),
      ve = vertices.end();
    int32_t index = 0;
    while ( v != ve )
    {

      vertexCoordinates[ index ++ ] = ( GLfloat )v->x;
      vertexCoordinates[ index ++ ] = ( GLfloat )v->y;
      vertexCoordinates[ index ++ ] = ( GLfloat )v->z;
      ++ v;

    }

    // creating vertex indices
    const std::list< gkg::Polygon< 3U > >&
      polygons = _sphereMeshMap.polygons.getPolygons( 0 );

    vertexIndices.resize( 3U * polygons.size() );

    std::list< gkg::Polygon< 3U > >::const_iterator
      p = polygons.begin(),
      pe = polygons.end();
    index = 0;
    while ( p != pe )
    {

      vertexIndices[ index ++ ] = ( GLuint )p->indices[ 0 ];
      vertexIndices[ index ++ ] = ( GLuint )p->indices[ 1 ];
      vertexIndices[ index ++ ] = ( GLuint )p->indices[ 2 ];
      ++ p;

    }

  }
  GKG_CATCH( "void gkg::AOdfFieldReader::getSphereVertexCoordinatesAndIndices( "
             "QVector< GLfloat >& vertexCoordinates, "
             "QVector<GLuint>& vertexIndices ) const" );

}


void gkg::AOdfFieldReader::getTransformationsAndTranslations(
      std::vector< std::vector< std::vector< GLfloat*  > > >& glTransformations,
      std::vector< std::vector< std::vector< QVector4D  > > >& glTranslations )
{

  try
  {

    std::cout << "computing transformations and translations..."
              << std::flush;

    gkg::TransformationsAndTranslationsProcessingContext
      context( _boundingBox,
               _outputOrientationCount, 
               _odfCartesianField->getResolution(),
               _transformations, 
               _translations,
               glTransformations,
               glTranslations );

    gkg::ThreadedLoop< int32_t >
      threadedLoop( &context,
                    0,
                    ( int32_t )_transformations.size() );
    threadedLoop.launch();

    std::cout << "done" << std::endl << std::flush;

  }
  GKG_CATCH( "void gkg::AOdfFieldReader::getTransformationsAndTranslations( "
             "std::vector< std::vector< std::vector< GLfloat*  > > >& "
             "transformations, "
             "std::vector< std::vector< std::vector< QVector4D  > > >& "
             "translations )" );

}


void gkg::AOdfFieldReader::getTransformationsAndTranslationsAndNormals( 
      std::vector< std::vector< std::vector< GLfloat*  > > >& glTransformations,
      std::vector< std::vector< std::vector< QVector4D  > > >& glTranslations,
      std::vector < std::vector < std::vector < QVector3D* > > >& glNormals )
{

  try
  {

    std::cout << "computing transformations, translations and normals..."
              << std::flush;

    gkg::TransformationsAndTranslationsAndNormalsProcessingContext
      context( _boundingBox,
               _outputOrientationCount,
               _sphereMeshMap,
               _odfCartesianField->getResolution(),
               _transformations, 
               _translations,
               glTransformations,
               glTranslations,
               glNormals );

    gkg::ThreadedLoop< int32_t >
      threadedLoop( &context,
                    0,
                    ( int32_t )_transformations.size() );
    threadedLoop.launch();

    std::cout << "done" << std::endl << std::flush;

  }
  GKG_CATCH( "void gkg::AOdfFieldReader::"
             "getTransformationsAndTranslationsAndNormals( "
             "std::vector< std::vector< std::vector< GLfloat*  > > >& "
             "transformations, "
             "std::vector< std::vector< std::vector< QVector4D  > > >& "
             "translations, "
             "std::vector < std::vector < std::vector < QVector3D* > > >& "
             "normals )" );

}


void gkg::AOdfFieldReader::getNormals(
          std::vector < std::vector < std::vector < QVector3D * > > >& normals )
{

  try
  {

    std::cout << "processing normals..." << std::flush;

    gkg::NormalsProcessingContext
      context( _boundingBox,
               _outputOrientationCount,
               _sphereMeshMap,
               _odfCartesianField->getResolution(),
               _transformations, 
               _translations,
               normals );

    gkg::ThreadedLoop< int32_t >
      threadedLoop( &context,
                    0,
                    ( int32_t )_transformations.size() );
    threadedLoop.launch();

    std::cout << "done" << std::endl << std::flush;

  }
  GKG_CATCH( "void gkg::AOdfFieldReader::getNormals( "
             "std::vector < std::vector < std::vector < QVector3D * > > >& "
             "normals )" );

}


void gkg::AOdfFieldReader::computeOdfField()
{

  try
  {

    std::cout << "processing ODF field..." << std::flush;

    int32_t siteCount = _siteMap.getSiteCount( 0 );
    _transformations.resize( siteCount );
    _translations.resize( siteCount );

    gkg::AOdfFieldProcessingContext context( _odfCartesianField,
                                             _transformations,
                                             _translations,
                                             _meshScale,
                                             _hasGFANormalization );

    gkg::ThreadedLoop< int32_t >
      threadedLoop( &context,
                    0,
                    siteCount );
    threadedLoop.launch();

    std::cout << "done" << std::endl << std::flush;

  }
  GKG_CATCH( "void gkg::AOdfFieldReader::computeOdfField()" );

}


void gkg::AOdfFieldReader::computeBoundingBox()
{

  try
  {

    std::set< float > orderedX;
    std::set< float > orderedY;
    std::set< float > orderedZ;

    float x = 0;
    float y = 0;
    float z = 0;
    std::vector< gkg::Translation3d< float > >::const_iterator
      t = _translations.begin(),
      te = _translations.end();
    while ( t != te )
    {

      t->getDirectTranslation( x, y, z );
      orderedX.insert( x );
      orderedY.insert( y );
      orderedZ.insert( z );
      ++ t;

    }
 
    _boundingBox.setLowerX( *orderedX.begin() );
    _boundingBox.setLowerY( *orderedY.begin() );
    _boundingBox.setLowerZ( *orderedZ.begin() );

    _boundingBox.setUpperX( *orderedX.rbegin() );
    _boundingBox.setUpperY( *orderedY.rbegin() );
    _boundingBox.setUpperZ( *orderedZ.rbegin() );

  }
  GKG_CATCH( "void gkg::AOdfFieldReader::computeBoundingBox()" );

}

