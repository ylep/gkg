#ifndef _gkg_anatomist_plugin_io_AOdfFieldReader_h_
#define _gkg_anatomist_plugin_io_AOdfFieldReader_h_


#include <gkg-processing-container/BoundingBox.h>
#include <gkg-processing-container/SiteMap.h>
#include <gkg-processing-container/TextureMap.h>
#include <gkg-processing-container/MeshMap.h>
#include <gkg-dmri-odf/OrientationDistributionFunction.h>
#include <gkg-processing-coordinates/OrientationSet.h>
#include <gkg-processing-transform/Translation3d.h>
#include <gkg-processing-coordinates/Vector3d.h>
#include <gkg-dmri-container/OdfCartesianField.h>

#include <QVector3D>
#include <QVector4D>
#include <QtOpenGL>

#include <vector>


namespace gkg
{


class AOdfFieldReader
{

  public:

    AOdfFieldReader( const std::string& fileNameSiteMap,
                     const std::string& fileNameTextureMap,
                     int32_t outputOrientationCount );
    virtual ~AOdfFieldReader();

    Vector3d< double > getResolution() const;
    const BoundingBox< float >& getBoundingBox() const;
    bool isSphericalHarmonics() const;
    bool hasGFANormalization() const;
    int32_t getOutputOrientationCount() const;

    void getSphereVertexCoordinatesAndIndices(
                                         QVector< GLfloat >& vertexCoordinates,
                                         QVector<GLuint>& vertexIndices ) const;

    void getTransformationsAndTranslations(
      std::vector< std::vector< std::vector< GLfloat*  > > >& transformations,
      std::vector< std::vector< std::vector< QVector4D  > > >& translations );
    void getTransformationsAndTranslationsAndNormals( 
      std::vector< std::vector< std::vector< GLfloat*  > > >& transformations,
      std::vector< std::vector< std::vector< QVector4D  > > >& translations,
      std::vector < std::vector < std::vector < QVector3D* > > >& normals );
    void getNormals(
      std::vector < std::vector < std::vector < QVector3D * > > >& normals );


  protected:

    void computeOdfField();
    void computeBoundingBox();

    BoundingBox< float > _boundingBox;
    int32_t _outputOrientationCount;
    bool _isSphericalHarmonics;
    bool _hasGFANormalization;
    float _meshScale;


    SiteMap< int32_t, int32_t > _siteMap;
    TextureMap< OrientationDistributionFunction > _odfs;
    OrientationSet* _outputOrientationSet;
    MeshMap< int32_t, float, 3U > _sphereMeshMap;

    gkg::OdfCartesianField* _odfCartesianField;

    std::vector< std::vector< float > > _transformations;
    std::vector< Translation3d< float > > _translations;

};



}


#endif
