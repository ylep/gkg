#ifndef _gkg_simulation_kokkos_DiffusionProcessKernels_i_h_
#define _gkg_simulation_kokkos_DiffusionProcessKernels_i_h_


#include <gkg-simulation-kokkos/DiffusionProcessKernels.h>
#include <gkg-simulation-virtual-tissue/AtomMethodFactory.h>
#include <gkg-core-exception/Exception.h>


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// struct VirtualTissueKokkosContainer
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


inline
gkg::VirtualTissueKokkosContainer::VirtualTissueKokkosContainer(
                                int32_t theAtomCount,
                                UInt8VectorView theAtomTypes,
                                BoolVectorView theAtomCompressedRepresentations,
                                FloatVectorView theAtomParameters,
                                UInt64VectorView theAtomParameterOffsets,
                                Int32VectorView theAtomPopulationIds,
                                Int32VectorView theAtomCellIds,
                                Int32VectorView theAtomCountPerGridVoxel,
                                Int32VectorView theAtomOffsetPerGridVoxel,
                                Int32VectorView theAtomIndexLut,
                                const gkg::BoundingBox< float >& theFieldOfView,
                                const Vector3d< double >& theGridResolutionInUm,
                                int32_t theAtomLutSizeX,
                                int32_t theAtomLutSizeY,
                                int32_t theAtomLutSizeZ,
                                int32_t theAtomLutSizeXY,
                                int32_t theAtomLutSizeXYZ,
                                int32_t theMaximumAtomCountPerGridVoxel )
                                  : atomCount( theAtomCount ),
                                    atomTypes( theAtomTypes ),
                                    atomCompressedRepresentations(
                                             theAtomCompressedRepresentations ),
                                    atomParameters( theAtomParameters ),
                                    atomParameterOffsets(
                                                      theAtomParameterOffsets ),
                                    atomPopulationIds( theAtomPopulationIds ),
                                    atomCellIds( theAtomCellIds ),
                                    atomCountPerGridVoxel(
                                                     theAtomCountPerGridVoxel ),
                                    atomOffsetPerGridVoxel(
                                                    theAtomOffsetPerGridVoxel ),
                                    atomIndexLut( theAtomIndexLut ),
                                    fieldOfView( theFieldOfView ),
                                    gridResolutionInUmX(
                                      ( float )theGridResolutionInUm.x ),
                                    gridResolutionInUmY(
                                      ( float )theGridResolutionInUm.y ),
                                    gridResolutionInUmZ(
                                      ( float )theGridResolutionInUm.z ),
                                    atomLutSizeX( theAtomLutSizeX ),
                                    atomLutSizeY( theAtomLutSizeY ),
                                    atomLutSizeZ( theAtomLutSizeZ ),
                                    atomLutSizeXY( theAtomLutSizeXY ),
                                    atomLutSizeXYZ( theAtomLutSizeXYZ ),
                                    maximumAtomCountPerGridVoxel(
                                               theMaximumAtomCountPerGridVoxel )
{

  try
  {

    fieldOfViewLowerX = fieldOfView.getLowerX();
    fieldOfViewLowerY = fieldOfView.getLowerY();
    fieldOfViewLowerZ = fieldOfView.getLowerZ();

    fieldOfViewSpanX = fieldOfView.getSpanX();
    fieldOfViewSpanY = fieldOfView.getSpanY();
    fieldOfViewSpanZ = fieldOfView.getSpanZ();

  }
  GKG_CATCH( "inline "
             "gkg::VirtualTissueKokkosContainer:: "
             "VirtualTissueKokkosContainer( "
             "int32_t theAtomCount, "
             "UInt8VectorView theAtomTypes, "
             "BoolVectorView theAtomCompressedRepresentations, "
             "FloatVectorView theAtomParameters, "
             "UInt64VectorView theAtomParameterOffsets, "
             "Int32VectorView theAtomPopulationIds, "
             "Int32VectorView theAtomCellIds, "
             "Int32VectorView theAtomCountPerGridVoxel, "
             "Int32VectorView theAtomOffsetPerGridVoxel, "
             "Int32VectorView theAtomIndexLut, "
             "const gkg::BoundingBox< float >& theFieldOfView, "
             "const Vector3d< double >& theGridResolutionInUm, "
             "int32_t theAtomLutSizeX, "
             "int32_t theAtomLutSizeY, "
             "int32_t theAtomLutSizeZ, "
             "int32_t theAtomLutSizeXY, "
             "int32_t theAtomLutSizeXYZ, "
             "int32_t theMaximumAtomCountPerGridVoxel )" );

}


inline
gkg::VirtualTissueKokkosContainer::~VirtualTissueKokkosContainer()
{
}


KOKKOS_INLINE_FUNCTION
void gkg::VirtualTissueKokkosContainer::getAtomLutIndices(
                                                          float coordinateX,
                                                          float coordinateY,
                                                          float coordinateZ,
                                                          int* atomIndices,
                                                          int& atomCount ) const
{

  try
  {

    int32_t atomLutX = std::min( 
                         std::max( 
                           ( int32_t )( coordinateX /
                                        gridResolutionInUmX ),
                           ( int32_t )0 ),
                         atomLutSizeX - 1 );
    int32_t atomLutY = std::min(
                         std::max( 
                           ( int32_t )( coordinateY /
                                        gridResolutionInUmY ),
                           ( int32_t )0 ),
                         atomLutSizeY - 1 );
    int32_t atomLutZ = std::min( 
                         std::max( 
                           ( int32_t )( coordinateZ /
                                        gridResolutionInUmZ ),
                           ( int32_t )0 ),
                         atomLutSizeZ - 1 );

    int32_t lutVoxel = atomLutX + 
                       atomLutY * atomLutSizeX + 
                       atomLutZ * atomLutSizeXY;

    int32_t lutOffset = atomOffsetPerGridVoxel( lutVoxel );

    atomCount = atomCountPerGridVoxel( lutVoxel );

    int32_t index = 0;
    for ( index = 0; index < atomCount; index++ )
    {

      atomIndices[ index ] = atomIndexLut( lutOffset + index );

    }

  }
  GKG_CATCH( "void gkg::VirtualTissueKokkosContainer::"
             "getAtomLUTIndices( "
             "float coordinateX, "
             "float coordinateY, "
             "float coordinateZ, "
             "int* atomIndices, "
             "int& atomCount ) const" );

}


KOKKOS_INLINE_FUNCTION
bool gkg::VirtualTissueKokkosContainer::belongToAtom( float coordinateX,
                                                      float coordinateY,
                                                      float coordinateZ,
                                                      int32_t atomIndex ) const
{

  try
  {

    return ( *gkg::AtomMethodFactory::getInstance().getBelongToMethod(
               atomTypes( atomIndex ) ) )(
                 &atomParameters( atomParameterOffsets( atomIndex ) ),
                 atomCompressedRepresentations( atomIndex ),
                 coordinateX,
                 coordinateY,
                 coordinateZ );                 

  }
  GKG_CATCH( "bool gkg::VirtualTissueKokkosContainer::"
             "belongToAtom( "
             "float coordinateX, "
             "float coordinateY, "
             "float coordinateZ, "
             "int32_t atomIndex ) const" );

}


KOKKOS_INLINE_FUNCTION
bool gkg::VirtualTissueKokkosContainer::inBox( const float& coordinateX,
                                               const float& coordinateY,
                                               const float& coordinateZ ) const
{

  try
  {

    if ( ( coordinateX < 0.0 ) ||
         ( coordinateY < 0.0 ) ||
         ( coordinateZ < 0.0 ) ||
         ( coordinateX > fieldOfViewSpanX ) ||
         ( coordinateY > fieldOfViewSpanY ) ||
         ( coordinateZ > fieldOfViewSpanZ ) )
    {

      return false;

    }
    return true;

  }
  GKG_CATCH( "void gkg::VirtualTissueKokkosContainer::inBox( "
             "const float& coordinateX, "
             "const float& coordinateY, "
             "const float& coordinateZ ) const" );

}


KOKKOS_INLINE_FUNCTION
float gkg::VirtualTissueKokkosContainer::getDistanceToMembrane(
                                                       float coordinateX,
                                                       float coordinateY,
                                                       float coordinateZ,
                                                       int32_t atomIndex ) const
{

  try
  {


    return ( *gkg::AtomMethodFactory::getInstance().
               getDistanceToMembraneMethod(
                 atomTypes( atomIndex ) ) )(
                   &atomParameters( atomParameterOffsets( atomIndex ) ),
                   atomCompressedRepresentations( atomIndex ),
                   coordinateX,
                   coordinateY,
                   coordinateZ );

  }
  GKG_CATCH( "float gkg::VirtualTissueKokkosContainer::getDistanceToMembrane( "
             "float coordinateX, "
             "float coordinateY, "
             "float coordinateZ, "
             "int32_t atomIndex ) const" );

}



////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// struct DiffusionProcessParticleInitializer
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

inline
gkg::DiffusionProcessParticleInitializer::DiffusionProcessParticleInitializer(
                         gkg::DiffusionProcessParticleInitializer::Type theType,
                         gkg::RCPointer< gkg::VirtualTissueKokkosContainer >
                                                theVirtualTissueKokkosContainer,
                         Float3VectorView theParticleCoordinates,
                         Int32VectorView theParticleIds,
                         Kokkos::Random_XorShift64_Pool<
                                   Kokkos::DefaultExecutionSpace > theRandPool )
                                         : type( theType ),
                                           virtualTissueKokkosContainer(
                                              theVirtualTissueKokkosContainer ),
                                           particleCoordinates(
                                                       theParticleCoordinates ),
                                           particleIds( theParticleIds ),
                                           randPool( theRandPool )
{
}


inline
gkg::DiffusionProcessParticleInitializer::~DiffusionProcessParticleInitializer()
{
}


//
// particleId = -1 -> extra
//            = >0 -> intra
//

KOKKOS_INLINE_FUNCTION
void gkg::DiffusionProcessParticleInitializer::operator()( 
                                                   int32_t particleIndex ) const
{

  try
  {

    // get a random number state from the pool for the active thread
    Kokkos::Random_XorShift64_Pool<
                                 Kokkos::DefaultExecutionSpace >::generator_type
      randomGenerator = randPool.get_state();

    float& coordinateX = particleCoordinates( particleIndex, 0 );
    float& coordinateY = particleCoordinates( particleIndex, 1 );
    float& coordinateZ = particleCoordinates( particleIndex, 2 );
    int32_t& particleId = particleIds( particleIndex );

    if ( type == gkg::DiffusionProcessParticleInitializer::Everywhere )
    {

      // randomly picking new coordinates
      coordinateX = randomGenerator.frand(
                               virtualTissueKokkosContainer->fieldOfViewSpanX );
      coordinateY = randomGenerator.frand(
                               virtualTissueKokkosContainer->fieldOfViewSpanY );
      coordinateZ = randomGenerator.frand(
                               virtualTissueKokkosContainer->fieldOfViewSpanZ );

      // allocating array of atom indices
      int32_t atomCount = 0;
      int32_t* atomIndices = new int32_t[ 
                   virtualTissueKokkosContainer->maximumAtomCountPerGridVoxel ];

      // collecting the atoms located close to the particle
      virtualTissueKokkosContainer->getAtomLutIndices( coordinateX,
                                                       coordinateY,
                                                       coordinateZ,
                                                       atomIndices,
                                                       atomCount );

      // checking whether the particle lies within one of the atom(s)
      particleId = -1;
      int32_t atomIndex = 0;
      for ( atomIndex = 0;
            atomIndex < atomCount;
            atomIndex++ )
      {

        if ( virtualTissueKokkosContainer->belongToAtom(
                                                    coordinateX,
                                                    coordinateY,
                                                    coordinateZ,
                                                    atomIndices[ atomIndex ] ) )
        {

          particleId = atomIndices[ atomIndex ];
          break;

        }

      }

      delete [] atomIndices;

    }
    else if ( type ==
                   gkg::DiffusionProcessParticleInitializer::OnlyIntracellular )
    {

      // looping while the chosen particle is not intracellular
      do
      {

        // randomly picking new coordinates
        coordinateX = randomGenerator.frand(
                               virtualTissueKokkosContainer->fieldOfViewSpanX );
        coordinateY = randomGenerator.frand(
                               virtualTissueKokkosContainer->fieldOfViewSpanY );
        coordinateZ = randomGenerator.frand(
                               virtualTissueKokkosContainer->fieldOfViewSpanZ );

        // allocating array of atom indices
        int32_t atomCount = 0;
        int32_t* atomIndices = new int32_t[ 
                   virtualTissueKokkosContainer->maximumAtomCountPerGridVoxel ];

        // collecting the atoms located close to the particle
        virtualTissueKokkosContainer->getAtomLutIndices( coordinateX,
                                                         coordinateY,
                                                         coordinateZ,
                                                         atomIndices,
                                                         atomCount );

        // checking whether the particle lies within one of the atom(s)
        particleId = -1;
        int32_t atomIndex = 0;
        for ( atomIndex = 0;
              atomIndex < atomCount;
              atomIndex++ )
        {

          if ( virtualTissueKokkosContainer->belongToAtom(
                                                    coordinateX,
                                                    coordinateY,
                                                    coordinateZ,
                                                    atomIndices[ atomIndex ] ) )
          {

            particleId = atomIndices[ atomIndex ];
            break;

          }

        }

        delete [] atomIndices;

      }
      while ( particleId == -1 );

    }
    else if ( type ==
                   gkg::DiffusionProcessParticleInitializer::OnlyExtracellular )
    {

      // looping while the chosen particle is not extracellular
      do
      {

        // randomly picking new coordinates
        coordinateX = randomGenerator.frand(
                               virtualTissueKokkosContainer->fieldOfViewSpanX );
        coordinateY = randomGenerator.frand(
                               virtualTissueKokkosContainer->fieldOfViewSpanY );
        coordinateZ = randomGenerator.frand(
                               virtualTissueKokkosContainer->fieldOfViewSpanZ );

        // allocating array of atom indices
        int32_t atomCount = 0;
        int32_t* atomIndices = new int32_t[ 
                   virtualTissueKokkosContainer->maximumAtomCountPerGridVoxel ];

        // collecting the atoms located close to the particle
        virtualTissueKokkosContainer->getAtomLutIndices( coordinateX,
                                                         coordinateY,
                                                         coordinateZ,
                                                         atomIndices,
                                                         atomCount );

        // checking whether the particle lies within one of the atom(s)
        particleId = -1;
        int32_t atomIndex = 0;
        for ( atomIndex = 0;
              atomIndex < atomCount;
              atomIndex++ )
        {

          if ( virtualTissueKokkosContainer->belongToAtom(
                                                    coordinateX,
                                                    coordinateY,
                                                    coordinateZ,
                                                    atomIndices[ atomIndex ] ) )
          {

            particleId = atomIndices[ atomIndex ];
            break;

          }

        }

        delete [] atomIndices;

      }
      while ( particleId != -1 );

    }

    // give the state back, which will allow another thread to aquire it
    randPool.free_state( randomGenerator );

  }
  GKG_CATCH( "void gkg::DiffusionProcessParticleInitializer::operator()( "
             "int32_t particleIndex ) const" );

}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// struct DiffusionProcessMonteCarloAndMRISimulator
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

inline
gkg::DiffusionProcessMonteCarloAndMRISimulator::
                                      DiffusionProcessMonteCarloAndMRISimulator(
      gkg::RCPointer< gkg::VirtualTissueKokkosContainer >
                                                theVirtualTissueKokkosContainer,
      FloatVectorView theCellPermeabilities,
      float theParticleDiffusivityInUm2PerUs,
      float theStepLength,
      int32_t theParticleCount,
      Float3VectorView theParticleCoordinates,
      Int32VectorView theParticleIds,
      Kokkos::Random_XorShift64_Pool< Kokkos::DefaultExecutionSpace >
                                                                    theRandPool,
      // for diffusion MRI signal attenuation simulation
      int32_t theMriSequenceCount,
      int32_t theTotalDiffusionWeightedContrastCount,
      FloatVectorView theAllSequencesPhaseShifts,

      Int32VectorView thePerContrastMriSequenceIndices,
      Int32VectorView thePerSequenceTimeStepCounts,
      Int32VectorView thePerSequencePhaseShiftStartingIndices,
      Int32VectorView thePerSequenceDiffusionWeightedContrastCounts,
      Int32VectorView theFormerAccumulatedDiffusionWeightedContrastCounts,

      const std::vector< FloatVectorView >&
                                              theAllSequencesPhaseAccumulators )
            : virtualTissueKokkosContainer( theVirtualTissueKokkosContainer ),
              cellPermeabilities( theCellPermeabilities ),
              particleDiffusivityInUm2PerUs( theParticleDiffusivityInUm2PerUs ),
              stepLength( theStepLength ),
              particleCount( theParticleCount ),
              particleCoordinates( theParticleCoordinates ),
              particleIds( theParticleIds ),
              randPool( theRandPool ),
              mriSequenceCount( theMriSequenceCount ),
              totalDiffusionWeightedContrastCount(
                                       theTotalDiffusionWeightedContrastCount ),
              allSequencesPhaseShifts( theAllSequencesPhaseShifts ),

              perContrastMriSequenceIndices( thePerContrastMriSequenceIndices ),
              perSequenceTimeStepCounts( thePerSequenceTimeStepCounts ),
              perSequencePhaseShiftStartingIndices(
                                      thePerSequencePhaseShiftStartingIndices ),
              perSequenceDiffusionWeightedContrastCounts(
                                thePerSequenceDiffusionWeightedContrastCounts ),
              formerAccumulatedDiffusionWeightedContrastCounts(
                          theFormerAccumulatedDiffusionWeightedContrastCounts ),
              allSequencesPhaseAccumulators( theAllSequencesPhaseAccumulators ),
              currentTimeStep( 0 )
{
}


inline
gkg::DiffusionProcessMonteCarloAndMRISimulator::
                                    ~DiffusionProcessMonteCarloAndMRISimulator()
{
}


KOKKOS_INLINE_FUNCTION
void gkg::DiffusionProcessMonteCarloAndMRISimulator::resetTimeStep()
{

  try
  {

    currentTimeStep = 0;

  }
  GKG_CATCH( "void gkg::DiffusionProcessMonteCarloAndMRISimulator::"
             "resetTimeStep()" );


}


KOKKOS_INLINE_FUNCTION
void gkg::DiffusionProcessMonteCarloAndMRISimulator::step()
{

  try
  {

    ++ currentTimeStep;

  }
  GKG_CATCH( "void gkg::DiffusionProcessMonteCarloAndMRISimulator::step()" );


}


//
// particleId = -1 -> extra
//            = >0 -> intra
//

KOKKOS_INLINE_FUNCTION
void gkg::DiffusionProcessMonteCarloAndMRISimulator::operator()( 
                                                   int32_t particleIndex ) const
{

  try
  {

    ////////////////////////////////////////////////////////////////////////////
    // first, trying to update the particle position
    ////////////////////////////////////////////////////////////////////////////


    float formerParticleCoordinateX = particleCoordinates( particleIndex, 0 );
    float formerParticleCoordinateY = particleCoordinates( particleIndex, 1 );
    float formerParticleCoordinateZ = particleCoordinates( particleIndex, 2 );

    float particleCoordinateX = formerParticleCoordinateX;
    float particleCoordinateY = formerParticleCoordinateY;
    float particleCoordinateZ = formerParticleCoordinateZ;
    int32_t& particleId = particleIds( particleIndex );

    bool isStepValid = true;

    // preallocating table of atom indices to speed-up method
    // size is twice the maximum atom count per LUT voxel because
    // the table will contain atoms from original and moved coordinates 
    int32_t* atomIndices = new int32_t[ 
               2 * virtualTissueKokkosContainer->maximumAtomCountPerGridVoxel ];

    float displacementDirectionX = 0.0f;
    float displacementDirectionY = 0.0f;
    float displacementDirectionZ = 0.0f;
    int32_t iteration = 0;
    do
    {

      // get a random number state from the pool for the active thread
      Kokkos::Random_XorShift64_Pool<
                                 Kokkos::DefaultExecutionSpace >::generator_type

      randomGenerator = randPool.get_state();

      displacementDirectionX = randomGenerator.normal( 0.0, stepLength );
      displacementDirectionY = randomGenerator.normal( 0.0, stepLength );
      displacementDirectionZ = randomGenerator.normal( 0.0, stepLength );

      // give the state back, which will allow another thread to aquire it
      randPool.free_state( randomGenerator );

      // dans le code de Kevin, il n'y a pas le sqrt .... a verifier
      float norm = std::sqrt( displacementDirectionX * displacementDirectionX +
                              displacementDirectionY * displacementDirectionY +
                              displacementDirectionZ * displacementDirectionZ );
      if ( norm > 0.0f )
      {

        displacementDirectionX /= norm;
        displacementDirectionY /= norm;
        displacementDirectionZ /= norm;

      }

      // incrementing temporary coordinates
      float displacementX = displacementDirectionX * stepLength;
      float displacementY = displacementDirectionY * stepLength;
      float displacementZ = displacementDirectionZ * stepLength;

      float crossingProbability = randomGenerator.frand();

      isStepValid = computeSubSteps( particleCoordinateX,
                                     particleCoordinateY,
                                     particleCoordinateZ,
                                     particleId,
                                     displacementX,
                                     displacementY,
                                     displacementZ,
                                     atomIndices,
                                     crossingProbability );
      ++ iteration;

    }
    while ( !isStepValid && ( iteration < 10 ) );



    // deleting preallocated table of atom indices
    delete [] atomIndices;

    // in case we get out of the box, we reset the particle coordinates to the
    // former ones
    if ( !virtualTissueKokkosContainer->inBox( particleCoordinateX,
                                               particleCoordinateY,
                                               particleCoordinateZ ) )
    {

      particleCoordinateX = formerParticleCoordinateX;
      particleCoordinateY = formerParticleCoordinateY;
      particleCoordinateZ = formerParticleCoordinateZ;

    }

    // setting the new coordinates if we stay within the box
    particleCoordinates( particleIndex, 0 ) = particleCoordinateX;
    particleCoordinates( particleIndex, 1 ) = particleCoordinateY;
    particleCoordinates( particleIndex, 2 ) = particleCoordinateZ;


    ////////////////////////////////////////////////////////////////////////////
    // second, updating the phase accumulator(s) for all sequence(s) accordingly
    ////////////////////////////////////////////////////////////////////////////

    float* allSequencesPhaseAccumulatorPtr = 
             &( allSequencesPhaseAccumulators[
                                             ( uint64_t )particleIndex ]( 0 ) );
    const float* allSequencesPhaseShiftPtr = 0;
    const int32_t* perContrastMriSequenceIndicesPtr =
      &perContrastMriSequenceIndices( 0 );

    int32_t diffusionWeightedContrastIndex = 0;
    uint64_t phaseShiftIndex = 0;
    for ( diffusionWeightedContrastIndex = 0;
          diffusionWeightedContrastIndex < totalDiffusionWeightedContrastCount;
          diffusionWeightedContrastIndex++ )
    {

      if ( currentTimeStep < perSequenceTimeStepCounts( 
                                           *perContrastMriSequenceIndicesPtr ) )
      {

        phaseShiftIndex = getPhaseShiftIndex( diffusionWeightedContrastIndex,
                                              currentTimeStep );
        
        allSequencesPhaseShiftPtr = &allSequencesPhaseShifts( phaseShiftIndex );

        *allSequencesPhaseAccumulatorPtr +=
         *allSequencesPhaseShiftPtr * particleCoordinateX +
         *( allSequencesPhaseShiftPtr + 1U ) * particleCoordinateY +
         *( allSequencesPhaseShiftPtr + 2U ) * particleCoordinateZ;


      }
      ++ allSequencesPhaseAccumulatorPtr;
      ++ perContrastMriSequenceIndicesPtr;

    }

  }
  GKG_CATCH( "void gkg::DiffusionProcessMonteCarloAndMRISimulator::operator()( "
             "int32_t particleIndex ) const" );

}


KOKKOS_INLINE_FUNCTION
uint64_t gkg::DiffusionProcessMonteCarloAndMRISimulator::getPhaseShiftIndex(
                                         int32_t diffusionWeightedContrastIndex,
                                         int32_t currentTimeStep ) const
{

  try
  {

    int32_t currentMriSequenceIndex = perContrastMriSequenceIndices(
                                               diffusionWeightedContrastIndex );
    int32_t forCurrentSequencePhaseShiftStartingIndex = 
                perSequencePhaseShiftStartingIndices( currentMriSequenceIndex );
    int32_t forCurrentSequenceDiffusionWeightedContrastCount =
                            perSequenceDiffusionWeightedContrastCounts(
                                                      currentMriSequenceIndex );
    int32_t forCurrentSequenceContrastIndex =
              diffusionWeightedContrastIndex - 
              formerAccumulatedDiffusionWeightedContrastCounts(
                                                      currentMriSequenceIndex );

    uint64_t forCurrentSequencePhaseShiftOffset =
             ( ( uint64_t )currentTimeStep * 
               ( uint64_t )forCurrentSequenceDiffusionWeightedContrastCount +
               ( uint64_t )forCurrentSequenceContrastIndex ) * 3U;

    return forCurrentSequencePhaseShiftStartingIndex +
           forCurrentSequencePhaseShiftOffset;

  }
  GKG_CATCH( "uint64_t gkg::DiffusionProcessMonteCarloAndMRISimulator::"
             "getPhaseShiftIndex( "
             "int32_t diffusionWeightedContrastIndex "
             "int32_t currentTimeStep ) const" );

}


KOKKOS_INLINE_FUNCTION
bool gkg::DiffusionProcessMonteCarloAndMRISimulator::computeSubSteps(
                                               float& particleCoordinateX,
                                               float& particleCoordinateY,
                                               float& particleCoordinateZ,
                                               int32_t& particleId,
                                               float& stepX,
                                               float& stepY,
                                               float& stepZ,
                                               int32_t* atomIndices,
                                               float crossingProbability ) const
{

  try
  {

    int32_t originalParticleId = particleId;
    int32_t updatedParticleId = particleId;
    float updateParticleCoordinateX = particleCoordinateX + stepX;
    float updateParticleCoordinateY = particleCoordinateY + stepY;
    float updateParticleCoordinateZ = particleCoordinateZ + stepZ;

    // if we go out of the virtual tissue box, it is not a valid step
    if ( !virtualTissueKokkosContainer->inBox( updateParticleCoordinateX,
                                               updateParticleCoordinateY,
                                               updateParticleCoordinateZ ) )
    {

      return false;

    }

    // allocating array of atom indices
    int32_t originalParticleAtomCount = 0;
    virtualTissueKokkosContainer->getAtomLutIndices(
                                                    particleCoordinateX,
                                                    particleCoordinateY,
                                                    particleCoordinateZ,
                                                    atomIndices,
                                                    originalParticleAtomCount );

    int32_t updatedParticleAtomCount = 0;
    virtualTissueKokkosContainer->getAtomLutIndices(
                                        updateParticleCoordinateX,
                                        updateParticleCoordinateY,
                                        updateParticleCoordinateZ,
                                        atomIndices + originalParticleAtomCount,
                                        updatedParticleAtomCount );

    int32_t atomCount = originalParticleAtomCount +
                        updatedParticleAtomCount;

    bool applyStep = false;

    // in case we were in the extracellular space
    if ( originalParticleId == -1 )
    {

      // by default we apply step when we come from extracellular space
      applyStep = true;

      int32_t atomIndex = 0;
      for ( atomIndex = 0;
            atomIndex < atomCount;
            atomIndex++ )
      {

        // in case we are about to enter the intracellular space
        if ( virtualTissueKokkosContainer->belongToAtom(
                                                    updateParticleCoordinateX,
                                                    updateParticleCoordinateY,
                                                    updateParticleCoordinateZ,
                                                    atomIndices[ atomIndex ] ) )
        {

          float cellPermeability = cellPermeabilities(
           virtualTissueKokkosContainer->atomCellIds( atomIndex ) );
          // if cell is permeable
          if ( cellPermeability > 0.0f )
          {

            float distanceFromParticleToMembrane = 
              virtualTissueKokkosContainer->getDistanceToMembrane(
                                                     particleCoordinateX,
                                                     particleCoordinateY,
                                                     particleCoordinateZ,
                                                     atomIndices[ atomIndex ] );
            float transmissionProbability = 2.0f *
                                            distanceFromParticleToMembrane *
                                            cellPermeability /
                                            ( 3.0f * 
                                              particleDiffusivityInUm2PerUs );
            if ( transmissionProbability > crossingProbability )
            {

              updatedParticleId = atomIndices[ atomIndex ];
              applyStep = true;
              break;

            }
            else
            {

              applyStep = false;
              break;

            }

          }
          // if cell is impermeable
          else
          {

            applyStep = false;
            break;

          }

        }

      }

    }
    // in case we were in the intracellular space
    else
    {

      int32_t atomIndex = 0;
      for ( atomIndex = 0;
            atomIndex < atomCount;
            atomIndex++ )
      {


        if ( virtualTissueKokkosContainer->belongToAtom(
                                                    updateParticleCoordinateX,
                                                    updateParticleCoordinateY,
                                                    updateParticleCoordinateZ,
                                                    atomIndices[ atomIndex ] ) )
        {

          // check if the particle has remained in its cell
          if ( ( virtualTissueKokkosContainer->atomPopulationIds( 
                                                   atomIndices[ atomIndex ] ) ==
                 virtualTissueKokkosContainer->atomPopulationIds( 
                                                               particleId ) ) &&
               ( virtualTissueKokkosContainer->atomCellIds( 
                                                   atomIndices[ atomIndex ] ) ==
                 virtualTissueKokkosContainer->atomCellIds( particleId ) ) )
          {

            updatedParticleId = atomIndices[ atomIndex ];
            applyStep = true;
            break;

          }
          // in case we are about to leave the intracellular space
          else
          {

            float cellPermeability = cellPermeabilities(
                      virtualTissueKokkosContainer->atomCellIds( particleId ) );

            if ( cellPermeability > 0.0f )
            {

              float distanceFromParticleToMembrane = 
              virtualTissueKokkosContainer->getDistanceToMembrane(
                                                     updateParticleCoordinateX,
                                                     updateParticleCoordinateY,
                                                     updateParticleCoordinateZ,
                                                     atomIndices[ atomIndex ] );
              float transmissionProbability = 2.0f *
                                               distanceFromParticleToMembrane *
                                               cellPermeability /
                                               ( 3.0f * 
                                                particleDiffusivityInUm2PerUs );
              if ( transmissionProbability > crossingProbability )
              {

                updatedParticleId = -1;
                applyStep = true;
                break;

              }
              else
              {

                applyStep = false;
                break;

              }

            }

          }

        }

      }

    }

    if ( applyStep ) 
    {

      particleCoordinateX = updateParticleCoordinateX;
      particleCoordinateY = updateParticleCoordinateY;
      particleCoordinateZ = updateParticleCoordinateZ;
      particleId = updatedParticleId;

    }

    return applyStep;

  }
  GKG_CATCH( "bool "
             "gkg::DiffusionProcessMonteCarloAndMRISimulator::computeSubSteps( "
             "float& particleCoordinateX, "
             "float& particleCoordinateY, "
             "float& particleCoordinateZ, "
             "int32_t& particleId, "
             "float& stepX, "
             "float& stepY, "
             "float& stepZ, "
             "int32_t* atomIndices, "
             "float crossingProbability ) const" );

}


#endif
